/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef HOMMINGMISSILECOMPONENT_HPP_
#define HOMMINGMISSILECOMPONENT_HPP_

#include <elixir/game/Component.hpp>
#include <elixir/math/Vector.hpp>
#include <Elixir/Config.hpp>
#include <limits>

class HommingMissileComponent final : public ee::Component
{
public:
  void SetTargetId(const ee::Id targetId)
  {
    targetId_ = targetId;
  }

  ee::Id GetTargetId() const
  {
    return targetId_;
  }

  void SetAutoDestructionTime(float time)
  {
    destructionTime_ = time;
  }

  float GetAutoDestructionTime() const
  {
    return destructionTime_;
  }

  void SetSpeed(float speed)
  {
    speed_ = speed;
  }

  float GetSpeed() const
  {
    return speed_;
  }

  void SetTimeTracking(float timeTracking)
  {
    timeTracking_ = timeTracking;
  }

  float GetTimeTracking() const
  {
    return timeTracking_;
  }

private:
  ee::Id targetId_ = ee::InvalidId;
  float destructionTime_ = 0.f;
  float speed_ = 0.f;
  float timeTracking_ = 0.f;
};

#endif