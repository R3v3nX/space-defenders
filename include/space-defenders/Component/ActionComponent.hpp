/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ACTIONCOMPONENT_HPP_
#define ACTIONCOMPONENT_HPP_

#include <elixir/game/Component.hpp>
#include <elixir/foundation/Array.hpp>
#include <elixir/math/Interpolator.hpp>

enum class ReverseMode
{
  OFF,
  ON
};
enum class Axis
{
  X,
  Y
};

class ActionComponent final : public ee::Component
{
public:

  void SetStartValue(float value, std::size_t position)
  {
    if (position < startValue_.size())
    {
      startValue_[position] = value;
    }
  }

  void SetStartValue(const ee::Array<float, 2>& startValue)
  {
    startValue_ = startValue;
  }

  const ee::Array<float, 2>& GetStartValue() const
  {
    return startValue_;
  }

  void SetEndValue(float value, std::size_t position)
  {
    if (position < endValue_.size())
    {
      endValue_[position] = value;
    }
  }

  void SetEndValue(const ee::Array<float, 2>& endValue)
  {
    endValue_ = endValue;
  }

  const ee::Array<float, 2>& GetEndValue() const
  {
    return endValue_;
  }

  void SetDurations(float value, std::size_t position)
  {
    if (position < durations_.size())
    {
      durations_[position] = value;
    }
  }

  void SetDurations(const ee::Array<float, 2>& durations)
  {
    durations_ = durations;
  }

  const ee::Array<float, 2>& GetDurations() const
  {
    return durations_;
  }

  void SetNumberOfRepetitions(float value, std::size_t position)
  {
    if (position < numberOfRepetitions_.size())
    {
      numberOfRepetitions_[position] = position;
    }
  }

  void SetNumberOfRepetitions(const ee::Array<unsigned, 2>& numberOfRepetitions)
  {
    numberOfRepetitions_ = numberOfRepetitions;
  }

  const ee::Array<unsigned, 2>& GetNumberOfRepetitions() const
  {
    return numberOfRepetitions_;
  }

  void SetElapsedTime(float time)
  {
    elapsedTime_ = time;
  }

  float GetElapsedTime() const
  {
    return elapsedTime_;
  }

  void SetCurrentMovementDelay(float delay)
  {
    currentMovementDelay_ = delay;
  }

  float GetCurrentMovementDelay()
  {
    return currentMovementDelay_;
  }

  void SetMovementDelay(float delay)
  {
    movementDelay_ = delay;
  }

  float GetMovementDelay() const
  {
    return movementDelay_;
  }

  void SetCurrentRepeat(unsigned value, std::size_t position)
  {
    if (position < repeats_.size())
    {
      repeats_[position] = value;
    }
  }

  void SetCurrentRepeat(const ee::Array<unsigned, 2>& repeats)
  {
    repeats_ = repeats;
  }

  const ee::Array<unsigned, 2>& GetCurrentRepeats() const
  {
    return repeats_;
  }

  const ee::Array<ee::EasingFunction<float>, 2>& GetEasingFunctions() const
  {
    return functions_;
  }

  void SetEasingFunctions(const ee::Array<ee::EasingFunction<float>, 2>& functions)
  {
    functions_ = functions;
  }

  void SetReverseModes(const ee::Array<ReverseMode, 2>& reverseModes)
  {
    reverseModes_ = reverseModes;
  }

  const ee::Array<ReverseMode, 2>& GetReverseMode() const
  {
    return reverseModes_;
  }

  void SetAxis(const ee::Array<Axis, 2>& axis)
  {
    axis_ = axis;
  }

  ee::Array<Axis, 2> GetAxis() const
  {
    return axis_;
  }

  void SetCurrentValue(const ee::Array<float, 2>& actualValue)
  {
    currentValue_ = actualValue;
  }

  void SetCurrentValue(float actualValue, std::size_t position)
  {
    if (position < currentValue_.size())
    {
      currentValue_[position] = actualValue;
    }
  }

  const ee::Array<float, 2>& GetCurrentValue() const
  {
    return currentValue_;
  }

  bool IsFinish() const
  {
    bool finish = true;

    for (std::size_t ax = 0; ax < axis_.size(); ax++)
    {
      finish &= (endValue_ == currentValue_) && (repeats_[ax] == numberOfRepetitions_[ax]);
    }

    return finish;
  }

private:
  ee::Array<float, 2> startValue_{};
  ee::Array<float, 2> currentValue_{};
  ee::Array<float, 2> endValue_{};
  ee::Array<float, 2> durations_{};
  ee::Array<unsigned, 2> numberOfRepetitions_{};
  ee::Array<unsigned, 2> repeats_{};
  ee::Array<ee::EasingFunction<float>, 2> functions_{};
  ee::Array<Axis, 2> axis_{};
  ee::Array<ReverseMode, 2> reverseModes_{};
  float elapsedTime_{};
  float currentMovementDelay_{};
  float movementDelay_{};
};

#endif