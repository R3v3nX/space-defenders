/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef BOUNDINGRECTCOMPONENT_HPP_
#define BOUNDINGRECTCOMPONENT_HPP_

#include <elixir/game/Component.hpp>

#include <elixir/math/Rectangle.hpp>
#include <elixir/math/Circle.hpp>

class CollisionComponent final : public ee::Component
{
public:
  void SetBoundingRect(const ee::Rectanglef& boundingRect)
  {
    rect_ = boundingRect;
  }

  const ee::Rectanglef& GetBoundingRect() const
  {
    return rect_;
  }

  ee::Rectanglef& GetBoundingRect()
  {
    return rect_;
  }

private:
  ee::Rectanglef rect_{};
};

#endif 