/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ACTORHOLDERCOMPONENT_HPP_
#define ACTORHOLDERCOMPONENT_HPP_

#include <elixir/game/Component.hpp>
#include <elixir/Foundation/Vector.hpp>

class ActorHolderComponent final : public ee::Component
{
public:
  void SetParent(ee::Id parent)
  {
    parent_ = parent;
  }

  ee::Id GetParent() const
  {
    return parent_;
  }

  void AddChild(ee::Id actor)
  {
    childrens_.push_back(actor);
  }

  const ee::Vector<ee::Id>& GetChildren() const
  {
    return childrens_;
  }

  ee::Vector<ee::Id>& GetChildren()
  {
    return childrens_;
  }

private:
  ee::Id parent_ = ee::InvalidId;
  ee::Vector<ee::Id> childrens_ = {};
};

#endif
