/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef CONTROLLERCOMPONENT_HPP_
#define CONTROLLERCOMPONENT_HPP_

#include <elixir/game/Component.hpp>
#include <elixir/math/Rect.hpp>

enum ButtonState
{
  PRESSED,
  RELEASED
};

class ControllerComponent final : public ee::Component
{
public:
  const ButtonState GetFireButtonState()
  {
    return fire_;
  }

  void SetFireButtonState(ButtonState buttonState)
  {
    fire_ = buttonState;
  }

  void SetSpeed(float speed)
  {
    speed_ = speed;
  }

  float GetSpeed() const
  {
    return speed_;
  }

private:
  float speed_ {};
  ButtonState fire_{ButtonState::RELEASED};
};

#endif