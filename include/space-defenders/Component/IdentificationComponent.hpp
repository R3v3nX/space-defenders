/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef IDENTIFICATIONCOMPONENT_HPP_
#define IDENTIFICATIONCOMPONENT_HPP_

#include <elixir/game/Component.hpp>

enum class Identifier
{
  Boss,
  Background,
  Meteor,
  MeteorLightShaft,
  Planet,
  Player,
  Missile,
  Rocks,
  SpaceShip,
  GameMaster,
  Animation,
  None
};

class IdentificationComponent final : public ee::Component
{
public:
  void SetIdentifier(Identifier identifier)
  {
    identifier_ = identifier;
  }

  Identifier GetIdentifier() const
  {
    return identifier_;
  }

  void MarkToDestroy()
  {
    destroy_ = true;
  }

  bool IsMarkedAsDestory() const
  {
    return destroy_;
  }
  
private:
  Identifier identifier_ = Identifier::None;
  bool destroy_ = false;
};

#endif