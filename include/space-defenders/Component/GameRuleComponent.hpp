/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef PLAYERCOMPONENT_HPP_
#define PLAYERCOMPONENT_HPP_

#include <elixir/game/Component.hpp>

class GameRuleComponent final : public ee::Component
{
public:
  void SetNumberOfPlayerLifes(unsigned lifes)
  {
    playerLifes_ = lifes;
  }

  unsigned GetNumberOfPlayerLifes() const
  {
    return playerLifes_;
  }

  void SetNumberOfMeteors(unsigned meteors)
  {
    numberOfMeteors_ = meteors;
  }

  unsigned GetNumberOfMeteors() const
  {
    return numberOfMeteors_;
  }

  void SetNumberOfSpaceShips(unsigned spaceShips)
  {
    numberOfSpaceShips_ = spaceShips;
  }

  unsigned GetNumberOfSpaceShips() const
  {
    return numberOfSpaceShips_;
  }

  void SetScore(unsigned score)
  {
    score_ = score;
  }

  unsigned GetScore() const
  {
    return score_;
  }

  void SetPlayerRespawnPosition(const ee::Vec3f& respawnPosition)
  {
    respawnPosition_ = respawnPosition;
  }

  const ee::Vec3f& GetPlayerRespawnPosition() const
  {
    return respawnPosition_;
  }

private:
  unsigned playerLifes_;
  unsigned numberOfMeteors_;
  unsigned numberOfSpaceShips_;
  unsigned score_;

  ee::Vec3f respawnPosition_{ 300.f, 750.f, 0.f };
};

#endif