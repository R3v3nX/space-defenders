/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ANIMATIONCOMPONENT_HPP_
#define ANIMATIONCOMPONENT_HPP_

#include <Elixir/Game/Component.hpp>

enum class AnimationState
{
  None,
  Start,
  Run,
  Stop,
  Pause,
  Finish
};

enum class AnimationType
{
  None,
  Death,
  Respawn,
  Custom
};

class AnimationComponent final: public ee::Component
{
public:
  void Start()
  {
    status_ = AnimationState::Start;
  }

  void Pause()
  {
    status_ = AnimationState::Pause;
  }

  void Stop()
  {
    status_ = AnimationState::Stop;
  }

  void Run()
  {
    status_ = AnimationState::Run;
  }

  void Finish()
  {
    status_ = AnimationState::Finish;
  }

  AnimationState GetState() const
  {
    return status_;
  }
  
  void SetType(AnimationType type)
  {
    type_ = type;
  }

  AnimationType GetType() const
  {
    return type_;
  }

protected:
  AnimationState status_ = AnimationState::None;
  AnimationType type_ = AnimationType::None;
  bool runState_ = false;
};

#endif
