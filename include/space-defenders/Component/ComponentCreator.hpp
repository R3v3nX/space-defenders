/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef COMPONENTCREATOR_HPP_
#define COMPONENTCREATOR_HPP_

#include <elixir/math/Vector.hpp>
#include <elixir/math/EasingFunctions.hpp>
#include <elixir/math/Rect.hpp>
#include <elixir/foundation/Array.hpp>
#include <elixir/foundation/String.hpp>
#include <elixir/Graphics/Material.hpp>
#include <elixir/game/Scene.hpp>
#include <elixir/game/Components/ColorTween.hpp>
#include <elixir/game/Components/MotionTween.hpp>
#include <elixir/game/Components/Sprite.hpp>

enum class Identifier;
class ParticleComponent;
class ControllerComponent;
class HommingMissileComponent;
class BoundingCircleComponent;
class CollisionComponent;
class ActionComponent;
class GameRuleComponent;
class AnimationComponent;
class ActorHolderComponent;

namespace ee
{
class Transform;
class Emitter;
}

namespace ComponentCreator
{
  void CreateSpriteComponent(ee::Scene& scene, ee::Actor actor, const ee::Sprite& properties);
  void CreateEmitterComponent(ee::Scene& scene, ee::Actor actor, const ee::Emitter& properties);
  void CreateIdentificationComponent(ee::Scene& scene, ee::Actor actor, Identifier id);
  void CreateTransformComponent(ee::Scene& scene, ee::Actor actor, const ee::Transform& properties);
  void CreateBoundingRect(ee::Scene& scene, ee::Actor actor, const CollisionComponent& properties);
  void CreateControllerComponent(ee::Scene& scene, ee::Actor actor, const ControllerComponent& properties);
  void CreateHomingMissileComponent(ee::Scene& scene, ee::Actor actor, const HommingMissileComponent& properties);
  void CreateActorHolderComponent(ee::Scene& scene, ee::Actor actor, const ActorHolderComponent& properties);
  void CreateAction(ee::Scene& scene, ee::Actor actor, const ActionComponent& properties);
  void CreateColorTween(ee::Scene& scene, ee::Actor actor, const ee::ColorTween& properties);
  void CreateMotionTween(ee::Scene& scene, ee::Actor actor, const ee::MotionTween& properties);
  void CreateGameComponent(ee::Scene& scene, ee::Actor actor, const GameRuleComponent& properties);
  void CreateAnimationComponent(ee::Scene& scene, ee::Actor actor, const AnimationComponent& properties);
};

#endif