/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef COMMANDS_HPP_
#define COMMANDS_HPP_

#include "ICommand.hpp"

#include <elixir/Game/Actor.hpp>
#include <elixir/math/Vector3.hpp>
#include <elixir/foundation/String.hpp>

namespace ee
{
  class Emitter;
  class Scene;
}

//ICommand CreatePlayer(ee::Scene& scene);
//ICommand CreateSpaceShip(ee::Scene& scene, const ee::String& materialName);
//ICommand CreateMeteor(ee::Scene& scene);
//ICommand CreateHommingMissile(ee::Scene& scene, const ee::Vec3f& position);
//ICommand StartRespawnAnimation(ee::Scene& scene, ee::Actor actor);
//ICommand StartDestroyAnimation(ee::Scene& scene, ee::Actor actor);
//ICommand DestroyActor(ee::Scene& scene, ee::Actor actor);

#endif