/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef CREATEACTORCOMMAND_HPP_ 
#define CREATEACTORCOMMAND_HPP_

#include "ICommand.hpp"

class IActorFactory;

class CreateActor final: public ICommand
{
public:
  CreateActor(IActorFactory& factory);
  void execute() override;

private:
  IActorFactory& factory_;
};

#endif