/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef DESTROYACTORCOMMAND_HPP_ 
#define DESTROYACTORCOMMAND_HPP_

#include "ICommand.hpp"

#include <Elixir/Game/Actor.hpp>
#include <Elixir/Game/Scene.hpp>

class DestroyActor final : public ICommand
{
public:
  DestroyActor(ee::Scene& scene, ee::Actor actor);
  void execute() override;

private:
  ee::Scene& scene_;
  ee::Actor actor_;
};

#endif