/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef COMMANDQUEUE_HPP_
#define COMMANDQUEUE_HPP_

#include "space-defenders/Command/ICommand.hpp"

#include <elixir/foundation/Stack.hpp>
#include <elixir/foundation/Pointer.hpp>

class CommandQueue final
{
public:
  CommandQueue() = default;
  ~CommandQueue() = default;

  static CommandQueue& GetInstance();

  void AddCommand(ee::UniquePtr<ICommand> command);
  ee::UniquePtr<ICommand> GetCommand();

  std::size_t GetSize() const;
  bool IsEmpty() const;

private:
  CommandQueue(const CommandQueue&) = delete;
  CommandQueue(CommandQueue&&) = delete;

  CommandQueue& operator=(const CommandQueue&) = delete;
  CommandQueue& operator=(CommandQueue&&) = delete;

  ee::Stack<ee::UniquePtr<ICommand>> queue_;
};

#endif
