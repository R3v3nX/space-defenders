/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef UTILITIES_HPP_
#define UTILITIES_HPP_

#include <elixir/game/Scene.hpp>

#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"

enum class AnimationType;

namespace Utilities
{
  using ConstIterator = ee::ActorList::ConstIterator;
  using Predicate = std::function <bool(const ee::Actor)>;

  ConstIterator FindActor(const ee::Scene& scene, Identifier id);
  ConstIterator FindActor(const ee::Scene& scene, const Predicate& predicate);

  void FindActors(ee::Vector<ee::Actor>& actors, ee::Scene& scene, Identifier id);
  void FindActors(ee::Vector<ee::Actor>& actors, ee::Scene& scene, const Predicate& predicate);

  ee::Id GetAnimationFromParent(const ee::Scene& scene, ee::Actor parent, AnimationType animationType);
};

#endif