/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef IGAMEACTORCREATOR_HPP_
#define IGAMEACTORCREATOR_HPP_

class IActorFactory
{
public:
  IActorFactory() = default;
  virtual ~IActorFactory() = default;

  IActorFactory(const IActorFactory&) = delete;
  IActorFactory(const IActorFactory&&) = delete;

  IActorFactory& operator=(const IActorFactory&) = delete;
  IActorFactory& operator=(const IActorFactory&&) = delete;

  virtual void Create() = 0;
};

#endif