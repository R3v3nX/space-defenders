/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef SPACESHIPFACTORY_HPP_
#define SPACESHIPFACTORY_HPP_

#include "space-defenders/Factory/IActorFactory.hpp"

#include <elixir/game/Scene.hpp>
#include <elixir/math/Vector3.hpp>
#include <elixir/foundation/String.hpp>

class SpaceShipFactory final : public IActorFactory
{
public:
  SpaceShipFactory(ee::Scene& scene);
  void Create() override;

private:
  ee::Actor CreateAnimation(const ee::Vec3f&);
  ee::Actor CreateSpaceShip(const ee::Vec3f&);
  
  const ee::String materialName_ = "SpaceShip.emt";
  ee::Scene& scene_;
};

#endif