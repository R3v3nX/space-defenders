/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef MISSILEFACTORY_HPP_
#define MISSILEFACTORY_HPP_

#include "space-defenders/Factory/IActorFactory.hpp"

#include <elixir/game/Scene.hpp>
#include <elixir/math/Vector.hpp>
#include <elixir/foundation/String.hpp>

class MissileFactory final : public IActorFactory
{
public:
  MissileFactory(ee::Scene& scene);

  void SetSpeed(float speed);
  void SetStartDelay(float delay);
  void SetDetonationTime(float time);
  void SetStartPosition(const ee::Vec3f& position);
  void SetTrackingTime(float time);
  void Create() override;

private:
  ee::Actor CreateAnimation();
  ee::Actor CreateMissile();

  const ee::String materialName_ = "Missile.emt";
  ee::Scene& scene_;
  ee::Vec3f startPosition_ = {};

  float speed_ = 0.f;
  float delay_ = 0.f;
  float detonationTime_ = 0.f;
  float trackingTime_ = 0.f;
};

#endif