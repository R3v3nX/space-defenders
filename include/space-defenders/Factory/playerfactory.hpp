/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef PLAYERFACTORY_HPP_
#define PLAYERFACTORY_HPP_

#include "space-defenders/Factory/IActorFactory.hpp"

#include <elixir/game/Scene.hpp>
#include <elixir/math/Vector.hpp>
#include <elixir/foundation/String.hpp>

class PlayerFactory final : public IActorFactory
{
public:
  PlayerFactory(ee::Scene& scene);
  void Create() override;

private:
  ee::Actor CreatePlayer(const ee::Vec3f&);
  ee::Actor CreateDeathAnimation(const ee::Vec3f&);
  ee::Actor CreateRespawnAnimation(const ee::Vec3f&);

  ee::Scene& scene_;
  const ee::String materialName_ = "Player.emt";
};

#endif