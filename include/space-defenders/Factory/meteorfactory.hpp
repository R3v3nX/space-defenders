/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef METEORFACTORY_HPP_
#define METEORFACTORY_HPP_

#include "space-defenders/Factory/IActorFactory.hpp"

#include <elixir/foundation/Array.hpp>
#include <elixir/foundation/Utility.hpp>
#include <elixir/foundation/String.hpp>
#include <elixir/game/Scene.hpp>
#include <elixir/math/Vector3.hpp>

class MeteorFactory final : public IActorFactory
{
public:
  MeteorFactory(ee::Scene& scene);
  void Create() override; 
  
private:
  ee::Actor CreateMeteor(const ee::Pair<ee::Vec3f, ee::Vec3f>&, float, float);
  ee::Actor CreateLightShaft(const ee::Vec3f&, float, float);
  ee::Actor CreateAnimation();

  static const ee::Array<const ee::Pair<ee::Vec3f, ee::Vec3f>, 10> MeteorSpawnPoints;
  const ee::String lightShaftMaterialName_ = "LightShaft.emt";
  const ee::String meteorMaterialName_ = "Meteor.emt";
  ee::Scene& scene_;
};

#endif