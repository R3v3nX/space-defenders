/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef WORLD_HPP_
#define WORLD_HPP_

#include <elixir/game/Director.hpp>
#include <elixir/game/Scene.hpp>
#include <elixir/game/SpriteSystem.hpp>
#include <elixir/game/ParticleSystem.hpp>
#include <elixir/game/TweenSystem.hpp>

#include <elixir/graphics/OrthoCamera.hpp>

#include "space-defenders/System/CommandSystem.hpp"
#include "space-defenders/System/ControllerSystem.hpp"
#include "space-defenders/System/ActionSystem.hpp"
#include "space-defenders/System/CollisionSystem.hpp"
#include "space-defenders/System/ActorCreatorSystem.hpp"
#include "space-defenders/System/MissileControlSystem.hpp"
#include "space-defenders/System/AnimationSystem.hpp"
#include "space-defenders/System/ActorCleanupSystem.hpp"
#include "space-defenders/System/ActorHolderSystem.hpp"

class World final
{
public:
  World(float designWidth, float designHeight, float windowWidth, float windowHeight);

  void Simulate();

private:
  void SetUpScene();
  void SetUpSystems();
  void SetUpCamera();
  void SetUpActors();
  void SetUpGameMaster();
  void SetUpBackground();
  void SetUpRocks();
  void SetUpPlanet();

  float designWidth_;
  float designHeight_;

  float windowWidth_;
  float windowHeight_;

  ee::Director director_;
  ee::Scene scene_;
  ee::OrthoCamera camera_;

  CommandSystem commandSystem_;
  ControllerSystem controllerSystem_;
  MissleControlSystem missileControlSystem_;
  ActionSystem actionSystem_;
  ActorHolderSystem actorHolderSystem_;
  CollisionSystem collisionSystem_;
  ActorCleanupSystem actorCleanupSystem_;
  ActorCreatorSystem actorCreatorSystem_;
  AnimationSystem animationSystem_;

  ee::SpriteSystem spriteSystem_;
  ee::ParticleSystem particleSystem_;
  ee::TweenSystem tweenSystem_;
};

#endif