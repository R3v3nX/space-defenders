/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ACTIONSYSTEM_HPP_
#define ACTIONSYSTEM_HPP_

#include <elixir/game/SimulationSystem.hpp>
#include <elixir/math/Interpolator.hpp>

class ActionComponent;
class IdentificationComponent;

class ActionSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene&, float) override;

private:
  void SetMovmentDelay(ActionComponent& action, float dt);
  void StartAction(ee::Scene&, ActionComponent&, float);
  void SetTranslation(ee::Interpolatorf&, ActionComponent&, ee::Scene&, std::size_t);
  void SwapValues(const ee::Interpolatorf&, ActionComponent&, std::size_t);
  void SetElapsedTime(const ee::Interpolatorf&, ActionComponent&, float, std::size_t);
  void SetCurrentRepeat(const ee::Interpolatorf&, ActionComponent&, std::size_t);
  void MarkActorToDestory(ee::Scene&, ActionComponent&, IdentificationComponent&);
};

#endif