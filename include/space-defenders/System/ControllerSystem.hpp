/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef CONTROLLERSYSTEM_HPP_
#define CONTROLLERSYSTEM_HPP_

#include <elixir/hid/KeyboardListener.hpp>

#include <elixir/game/SimulationSystem.hpp>
#include <elixir/game/components/Transform.hpp>

#include <elixir/math/Vector3.hpp>
#include <elixir/math/Rect.hpp>

class ControllerComponent;

class ControllerSystem final : public ee::SimulationSystem, public ee::KeyboardListener
{
public:
  ControllerSystem(float worldHeight, float worldWidth);
  virtual ~ControllerSystem();

  void Step(ee::Scene&, float) override;
  void OnKeyPress(ee::Key key) override;
  void OnKeyRelease(ee::Key key) override;

private:
  void HandleMovement(ee::Scene&, ControllerComponent&, float);
  float CalculateNewPosition(float, float, ee::Array<float, 2>&);
  void HandleFire(ee::Scene&, ControllerComponent&);
  void SendCommandToCreateMissile(ee::Scene&, const ee::Vec3f&);

  bool moveUp_{ false };
  bool moveDown_{ false };
  bool moveLeft_{ false };
  bool moveRight_{ false };
  bool fire_{ false };

  float worldHeight_;
  float worldWidth_;
};

#endif