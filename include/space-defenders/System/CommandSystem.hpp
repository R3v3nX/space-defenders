/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef COMMANDSYSTEM_HPP_
#define COMMANDSYSTEM_HPP_

#include <elixir/game/SimulationSystem.hpp>

class CommandSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene &, float) override;
};
#endif