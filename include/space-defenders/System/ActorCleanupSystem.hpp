/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef CLEANUPACTORSYSTEM_HPP_
#define CLEANUPACTORSYSTEM_HPP_

#include <elixir/game/SimulationSystem.hpp>
#include <elixir/game/Actor.hpp>
#include <elixir/game/components/ColorTween.hpp>
#include <elixir/game/components/MotionTween.hpp>

class IdentificationComponent;

class ActorCleanupSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene&, float) override;

private:
  bool IsReadyToDestroy(ee::Scene&, IdentificationComponent&);
};

#endif