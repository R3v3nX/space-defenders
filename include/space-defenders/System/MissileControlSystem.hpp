/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef MISSILECONTROLRSYSTEM_HPP_
#define MISSILECONTROLRSYSTEM_HPP_

#include <elixir/game/SimulationSystem.hpp>
#include <elixir/game/Actor.hpp>
#include <elixir/foundation/Vector.hpp>
#include <elixir/math/Vector.hpp>

#include "space-defenders/Utilities.hpp"

class HommingMissileComponent;
class MissleControlSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene& scene, float dt) override;

private:
  void UpdateMissileTarget(ee::Scene&, HommingMissileComponent&, float);
  void UpdateMissilePosition(ee::Scene&, HommingMissileComponent&, float);
  void UpdateMissileDetonationTime(HommingMissileComponent&, float);
  void UpdateMissileTimeTracking(HommingMissileComponent&, float);
  void MarkToDestroyMissile(ee::Scene&, const HommingMissileComponent&);

  ee::Vec3f CalculateVelocity(ee::Scene&, HommingMissileComponent&, float);
  ee::Id FindClosestTarget(ee::Scene&, const ee::Vec3f&);
  float CalculateDistance(const ee::Vec3f&, const ee::Vec3f&);
};

#endif