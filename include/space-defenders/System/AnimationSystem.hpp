/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ANIMATIONSYSTEM_HPP_
#define ANIMATIONSYSTEM_HPP_

#include <elixir/game/SimulationSystem.hpp>
#include <elixir/game/Scene.hpp>

class AnimationComponent;

class AnimationSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene &, float) override;

private:
  void StartAnimation(ee::Scene&, AnimationComponent&);
  void SetAnimationVisibleState(ee::Scene&, ee::Actor, bool);

  template<typename T>
  void StartTweener(ee::Scene&, ee::Actor);

  void CheckAnimationStatus(ee::Scene&, AnimationComponent&);
  template<typename T>
  bool IsTweenerFinish(ee::Scene&, ee::Actor);

  void StopAnimation(ee::Scene&, AnimationComponent&);
};

template<typename T>
void AnimationSystem::StartTweener(ee::Scene& scene, ee::Actor actor)
{
  if (scene.HasComponent<T>(actor))
  {
    scene.GetComponent<T>(actor).Start();
  }
}

template<typename T>
bool AnimationSystem::IsTweenerFinish(ee::Scene& scene, ee::Actor actor)
{
  bool status = true;

  if (scene.HasComponent<T>(actor))
  {
    status = scene.GetComponent<T>(actor).IsStopped();
  }

  return status;
}

#endif