/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ENEMYCREATORSYSTEM_HPP_
#define ENEMYCREATORSYSTEM_HPP_

#include <elixir/game/SimulationSystem.hpp>

#include "space-defenders/Factory/MeteorFactory.hpp"
#include "space-defenders/Factory/MissileFactory.hpp"
#include "space-defenders/Factory/PlayerFactory.hpp"
#include "space-defenders/Factory/SpaceshipFactory.hpp"

enum class Identifier;
class GameRuleComponent;

class ActorCreatorSystem final : public ee::SimulationSystem
{
public:
  ActorCreatorSystem(ee::Scene&);
  void Step(ee::Scene&, float) override;

private:
  void SendCommandToCreateMeteors(ee::Scene&, const GameRuleComponent&);
  void SendCommandToCreateSpaceShips(ee::Scene&, const GameRuleComponent&);
  void SendCommandToCreateMissile(ee::Scene&);
  void SendCommandToCreatePlayer(ee::Scene&, GameRuleComponent&);
  std::size_t GetNumberOfEnemies(ee::Scene&, Identifier);

  MeteorFactory meteorFactor_;
  MissileFactory missileFactory_;
  PlayerFactory playerFactory_;
  SpaceShipFactory spaceShipFactory_;
};
#endif