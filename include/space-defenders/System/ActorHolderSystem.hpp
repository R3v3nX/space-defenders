/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ACTORHOLDERSYSTEM_HPP_
#define ACTORHOLDERSYSTEM_HPP_

#include <Elixir/Foundation/Vector.hpp>
#include <Elixir/Game/SimulationSystem.hpp>
#include <Elixir/Game/Scene.hpp>

class ActorHolderComponent;

class ActorHolderSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene&, float) override;

private:
  void UpdateChildrensVisible(ee::Scene&, const ActorHolderComponent&);
  void UpdateChildrensTransform(ee::Scene&, const ActorHolderComponent&);
  void UpdateChildrensMotion(ee::Scene&, const ActorHolderComponent&);
};

#endif