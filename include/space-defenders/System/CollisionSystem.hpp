/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Elixir Engine (c) 2015 Bartlomiej Parowicz
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef COLLISIONSYSTEM_HPP_
#define COLLISIONSYSTEM_HPP_

#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Utilities.hpp"

#include <elixir/game/SimulationSystem.hpp>
#include <elixir/game/ComponentList.hpp>

#include <elixir/foundation/Utility.hpp>

enum class Identifier;

class CollisionSystem final : public ee::SimulationSystem
{
public:
  void Step(ee::Scene&, float) override;

private:
  void UpdateBoundings(ee::Scene&);
  void FindColliders(ee::Scene&, ee::Vector<CollisionComponent>&, const ee::Vector<Identifier>&);
  void HandleCollision(ee::Scene&, CollisionComponent&, ee::Vector<CollisionComponent>&);
  bool IsCollision(const CollisionComponent&, const CollisionComponent&);
  void StartDestroyProcedure(ee::Scene& scene, ee::Actor actor);
};

#endif