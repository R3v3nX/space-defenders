#include "space-defenders/Component/ComponentCreator.hpp"
#include "space-defenders/Component/ControllerComponent.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/HomingMissileComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Component/GameRuleComponent.hpp"

#include <elixir/math/Rectangle.hpp>
#include <elixir/math/Circle.hpp>

#include <elixir/game/components/Sprite.hpp>
#include <elixir/game/components/Transform.hpp>
#include <elixir/game/components/Emitter.hpp>
#include <elixir/game/components/MotionTween.hpp>

#include <elixir/ContentManager.hpp>

namespace ComponentCreator
{
  template <typename T>
  void CreateComponent(ee::Scene &scene, ee::Actor actor)
  {
    if (!scene.HasComponent<T>(actor))
    {
      scene.CreateComponent<T>(actor);
    }
  }

  void CreateSpriteComponent(ee::Scene& scene, ee::Actor actor, const ee::Sprite& properties)
  {
    CreateComponent<ee::Sprite>(scene, actor);

    scene.GetComponent<ee::Sprite>(actor).SetAnchor(properties.GetAnchor());
    scene.GetComponent<ee::Sprite>(actor).SetColor(properties.GetColor());
    scene.GetComponent<ee::Sprite>(actor).SetMaterial(properties.GetMaterial());
    scene.GetComponent<ee::Sprite>(actor).SetVisible(properties.IsVisible());
  }

  void CreateEmitterComponent(ee::Scene& scene, ee::Actor actor, const ee::Emitter& properties)
  {
    CreateComponent<ee::Emitter>(scene, actor);

    scene.GetComponent<ee::Emitter>(actor).SetAcceleration(properties.GetAcceleration());
    scene.GetComponent<ee::Emitter>(actor).SetAccelerationVariation(properties.GetAccelerationVariation());
    scene.GetComponent<ee::Emitter>(actor).SetEmissionRate(properties.GetEmissionRate());
    scene.GetComponent<ee::Emitter>(actor).SetEndColor(properties.GetEndColor());
    scene.GetComponent<ee::Emitter>(actor).SetEndScale(properties.GetEndScale());
    scene.GetComponent<ee::Emitter>(actor).SetEndScaleVariation(properties.GetEndScaleVariation());
    scene.GetComponent<ee::Emitter>(actor).SetLifetime(properties.GetLifetime());
    scene.GetComponent<ee::Emitter>(actor).SetStartAngle(properties.GetStartAngle());
    scene.GetComponent<ee::Emitter>(actor).SetStartAngleVariation(properties.GetStartAngleVariation());
    scene.GetComponent<ee::Emitter>(actor).SetStartColor(properties.GetStartColor());
    scene.GetComponent<ee::Emitter>(actor).SetStartScale(properties.GetStartScale());
    scene.GetComponent<ee::Emitter>(actor).SetStartSpeed(properties.GetStartSpeed());
    scene.GetComponent<ee::Emitter>(actor).SetStartSpeedVariation(properties.GetStartSpeedVariation());
    scene.GetComponent<ee::Emitter>(actor).SetMaterial(properties.GetMaterial());
  }

  void CreateIdentificationComponent(ee::Scene& scene, ee::Actor actor, Identifier id)
  {
    CreateComponent<IdentificationComponent>(scene, actor);

    scene.GetComponent<IdentificationComponent>(actor).SetIdentifier(id);
  }

  void CreateTransformComponent(ee::Scene& scene, ee::Actor actor, const ee::Transform& properties)
  {
    CreateComponent<ee::Transform>(scene, actor);

    scene.GetComponent<ee::Transform>(actor).SetPosition(properties.GetPosition());
    scene.GetComponent<ee::Transform>(actor).SetRotation(properties.GetRotationAngle(), ee::Vec3f(0.f, 0.f, 1.f));
  }

  void CreateControllerComponent(ee::Scene& scene, ee::Actor actor, const ControllerComponent& properties)
  {
    CreateComponent<ControllerComponent>(scene, actor);

    scene.GetComponent<ControllerComponent>(actor).SetSpeed(properties.GetSpeed());
  }

  void CreateHomingMissileComponent(ee::Scene& scene, ee::Actor actor, const HommingMissileComponent& properties)
  {
    CreateComponent<HommingMissileComponent>(scene, actor);

    scene.GetComponent<HommingMissileComponent>(actor).SetSpeed(properties.GetSpeed());
    scene.GetComponent<HommingMissileComponent>(actor).SetTimeTracking(properties.GetTimeTracking());
    scene.GetComponent<HommingMissileComponent>(actor).SetAutoDestructionTime(properties.GetAutoDestructionTime());
  }

  void CreateBoundingRect(ee::Scene& scene, ee::Actor actor, const CollisionComponent& properties)
  {
    CreateComponent<CollisionComponent>(scene, actor);

    scene.GetComponent<CollisionComponent>(actor).SetBoundingRect(properties.GetBoundingRect());
  }

  void CreateActorHolderComponent(ee::Scene& scene, ee::Actor actor, const ActorHolderComponent& properties)
  {
    CreateComponent<ActorHolderComponent>(scene, actor);
   
    for (const auto& actorId : properties.GetChildren())
    {
      scene.GetComponent<ActorHolderComponent>(actor).AddChild(actorId);
    }

    scene.GetComponent<ActorHolderComponent>(actor).SetParent(properties.GetParent());
  }

  void CreateAction(ee::Scene& scene, ee::Actor actor, const ActionComponent& properties)
  {
    CreateComponent<ActionComponent>(scene, actor);

    scene.GetComponent<ActionComponent>(actor).SetStartValue(properties.GetStartValue());
    scene.GetComponent<ActionComponent>(actor).SetEndValue(properties.GetEndValue());
    scene.GetComponent<ActionComponent>(actor).SetDurations(properties.GetDurations());
    scene.GetComponent<ActionComponent>(actor).SetNumberOfRepetitions(properties.GetNumberOfRepetitions());
    scene.GetComponent<ActionComponent>(actor).SetMovementDelay(properties.GetMovementDelay());
    scene.GetComponent<ActionComponent>(actor).SetEasingFunctions(properties.GetEasingFunctions());
    scene.GetComponent<ActionComponent>(actor).SetAxis(properties.GetAxis());
    scene.GetComponent<ActionComponent>(actor).SetReverseModes(properties.GetReverseMode());
  }

  void CreateColorTween(ee::Scene& scene, ee::Actor actor, const ee::ColorTween& properties)
  {
    CreateComponent<ee::ColorTween>(scene, actor);

    scene.GetComponent<ee::ColorTween>(actor).SetTweenType(properties.GetTweenType());
    scene.GetComponent<ee::ColorTween>(actor).SetDuration(properties.GetDuration());
    scene.GetComponent<ee::ColorTween>(actor).SetStartColor(properties.GetStartColor());
    scene.GetComponent<ee::ColorTween>(actor).SetFinishColor(properties.GetFinishColor());
  }

  void CreateMotionTween(ee::Scene& scene, ee::Actor actor, const ee::MotionTween& properties)
  {
    CreateComponent<ee::MotionTween>(scene, actor);

    scene.GetComponent<ee::MotionTween>(actor).SetTweenType(properties.GetTweenType());
    scene.GetComponent<ee::MotionTween>(actor).SetDuration(properties.GetDuration());
    scene.GetComponent<ee::MotionTween>(actor).SetStartPosition(properties.GetStartPosition());
    scene.GetComponent<ee::MotionTween>(actor).SetFinishPosition(properties.GetFinishPosition());
    scene.GetComponent<ee::MotionTween>(actor).SetStartScale(properties.GetStartScale());
    scene.GetComponent<ee::MotionTween>(actor).SetFinishScale(properties.GetFinishScale());
  }

  void CreateGameComponent(ee::Scene& scene, ee::Actor actor, const GameRuleComponent& properties)
  {
    CreateComponent<GameRuleComponent>(scene, actor);

    scene.GetComponent<GameRuleComponent>(actor).SetNumberOfPlayerLifes(properties.GetNumberOfPlayerLifes());
    scene.GetComponent<GameRuleComponent>(actor).SetNumberOfSpaceShips(properties.GetNumberOfSpaceShips());
    scene.GetComponent<GameRuleComponent>(actor).SetNumberOfMeteors(properties.GetNumberOfMeteors());
    scene.GetComponent<GameRuleComponent>(actor).SetScore(properties.GetScore());
  }

  void CreateAnimationComponent(ee::Scene& scene, ee::Actor actor, const AnimationComponent& properties)
  {
    CreateComponent<AnimationComponent>(scene, actor);

    scene.GetComponent<AnimationComponent>(actor).SetType(properties.GetType());
  }
}