#include "space-defenders/System/MissileControlSystem.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Command/CommandQueue.hpp"
#include "space-defenders/Component/HomingMissileComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"
#include "space-defenders/Command/Commands.hpp"
#include "space-defenders/Utilities.hpp"

#include <elixir/math/Utility.hpp>
#include <elixir/game/Scene.hpp>
#include <elixir/game/components/Transform.hpp>
#include <elixir/game/components/Sprite.hpp>

void MissleControlSystem::Step(ee::Scene& scene, float dt)
{
  auto& hommingMissileComponents = scene.GetComponentList<HommingMissileComponent>();

  for (auto& missileComponent : hommingMissileComponents)
  {
    if (!scene.GetComponent<IdentificationComponent>(missileComponent.actor).IsMarkedAsDestory())
    {
      UpdateMissileTarget(scene, missileComponent, dt);
      UpdateMissilePosition(scene, missileComponent, dt);
      UpdateMissileDetonationTime(missileComponent, dt);
      UpdateMissileTimeTracking(missileComponent, dt);
      MarkToDestroyMissile(scene, missileComponent);
    }
  }
}

void MissleControlSystem::UpdateMissileTarget(ee::Scene& scene, HommingMissileComponent& missileComponent, float dt)
{
  if (missileComponent.GetTimeTracking() <= 0.f)
  {
    const auto& missilePosition = scene.GetComponent<ee::Transform>(missileComponent.actor).GetPosition();
    auto targetId = FindClosestTarget(scene, missilePosition);
    missileComponent.SetTargetId(targetId);
  }
}

ee::Id MissleControlSystem::FindClosestTarget(ee::Scene& scene, const ee::Vec3f& missilePosition)
{
  auto nearestTarget = ee::InvalidId;
  auto maxDistance = std::numeric_limits<float>::infinity();
  const auto& idComponents = scene.GetComponentList<IdentificationComponent>();

  for (const auto& idComp : idComponents)
  {
    if (idComp.GetIdentifier() == Identifier::SpaceShip && !idComp.IsMarkedAsDestory())
    {
      auto& targetPosition = scene.GetComponent<ee::Transform>(idComp.actor).GetPosition();
      auto distance = (targetPosition.x > 0.f && targetPosition.y > 0.f) ? CalculateDistance(missilePosition, targetPosition) : maxDistance;

      if (distance < maxDistance)
      {
        maxDistance = distance;
        nearestTarget = idComp.actor.GetId();
      }
    }
  }

  return nearestTarget;
}

float MissleControlSystem::CalculateDistance(const ee::Vec3f& position1, const ee::Vec3f& position2)
{
  return ee::Sqrt(ee::Pow2(position2.x - position1.x) + ee::Pow2(position2.y - position1.y));
}

void MissleControlSystem::UpdateMissilePosition(ee::Scene& scene, HommingMissileComponent& missileComponent, float dt)
{
  ee::Vec3f newVelocity = CalculateVelocity(scene, missileComponent, dt);
  float angleInRadians = static_cast<float>(-1 * atan2(newVelocity.x, newVelocity.y));

  auto& missileTransform = scene.GetComponent<ee::Transform>(missileComponent.actor);
  missileTransform.Translate(newVelocity);
  missileTransform.SetRotation(angleInRadians, { 0.f, 0.f, 1.f });
}

ee::Vec3f MissleControlSystem::CalculateVelocity(ee::Scene& scene, HommingMissileComponent& missileComponent, float dt)
{
  auto& missilePosition = scene.GetComponent<ee::Transform>(missileComponent.actor).GetPosition();
  
  const float approachRate = 200.f;
  auto id = missileComponent.GetTargetId();
  ee::Vec3f velocity = ee::Vec3f(approachRate * dt * 1.f, 0.f, 0.f);

  if (id != ee::InvalidId)
  {
    auto& targetPosition = scene.GetComponent<ee::Transform>(ee::Actor(id)).GetPosition();
    ee::Vec3f targetDirection = (targetPosition - missilePosition).GetNormalized();
    velocity = approachRate * dt * targetDirection;
  }

  return velocity * missileComponent.GetSpeed();
}

void MissleControlSystem::UpdateMissileDetonationTime(HommingMissileComponent& missileComponent, float dt)
{
  auto currentDetonationTime = (missileComponent.GetAutoDestructionTime() <= 0) ? 0.f : missileComponent.GetAutoDestructionTime() - dt;
  missileComponent.SetAutoDestructionTime(currentDetonationTime);
}

void MissleControlSystem::UpdateMissileTimeTracking(HommingMissileComponent& missileComponent, float dt)
{
  auto currentTrackingTime = (missileComponent.GetTimeTracking() <= 0 ) ? 0.f : missileComponent.GetTimeTracking() - dt;
  missileComponent.SetTimeTracking(currentTrackingTime);
}

void MissleControlSystem::MarkToDestroyMissile(ee::Scene& scene, const HommingMissileComponent& missileComponent)
{
  if (missileComponent.GetAutoDestructionTime() <= 0.f)
  {
    scene.GetComponent<IdentificationComponent>(missileComponent.actor).MarkToDestroy();
    auto animationId = Utilities::GetAnimationFromParent(scene, missileComponent.actor, AnimationType::Death);

    if (animationId != ee::InvalidId)
    {
      scene.GetComponent<AnimationComponent>(ee::Actor(animationId)).Start();
    }
  }
}