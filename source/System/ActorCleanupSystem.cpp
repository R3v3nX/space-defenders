#include "space-defenders/System/ActorCleanupSystem.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Command/CommandQueue.hpp"
#include "space-defenders/Command/DestroyActor.hpp"
#include "space-defenders/Utilities.hpp"

#include <elixir/game/Scene.hpp>

void ActorCleanupSystem::Step(ee::Scene& scene, float dt)
{
  auto& componentId = scene.GetComponentList<IdentificationComponent>();

  for (auto component : componentId)
  {
    if (IsReadyToDestroy(scene, component))
    {
      CommandQueue::GetInstance().AddCommand(ee::MakeUnique<DestroyActor>(scene, component.actor));
    }
  }
}

bool ActorCleanupSystem::IsReadyToDestroy(ee::Scene& scene, IdentificationComponent& component)
{
  bool destroy = component.IsMarkedAsDestory();
  if (destroy)
  {
    ee::Id animationId = Utilities::GetAnimationFromParent(scene, component.actor, AnimationType::Death);
    if (animationId != ee::InvalidId)
    {
      const auto& animationComponent = scene.GetComponent<AnimationComponent>(ee::Actor(animationId));
      destroy = (animationComponent.GetState() == AnimationState::Finish);
    }
  }
  return destroy;
}