#include "space-defenders/System/ActorHolderSystem.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"

#include <Elixir/Game/Components/MotionTween.hpp>
#include <Elixir/Game/Components/Sprite.hpp>
#include <Elixir/Game/Components/Transform.hpp>
#include <Elixir/Game/Scene.hpp>

void ActorHolderSystem::Step(ee::Scene& scene, float)
{
  auto& holderComponents = scene.GetComponentList<ActorHolderComponent>();

  for (auto& component : holderComponents)
  {
    UpdateChildrensVisible(scene, component);

    if (scene.HasComponent<ee::Transform>(component.actor))
    {
      UpdateChildrensTransform(scene, component);
      UpdateChildrensMotion(scene, component);
    }
  }
}

void ActorHolderSystem::UpdateChildrensVisible(ee::Scene& scene, const ActorHolderComponent& parentComponent)
{
  const auto& children = parentComponent.GetChildren();
  const auto& parentSpriteComp = scene.GetComponent<ee::Sprite>(parentComponent.actor);

  for (auto child : children)
  {
    if (scene.HasComponent<ee::Sprite>(ee::Actor(child)) && 
        scene.GetComponent<IdentificationComponent>(ee::Actor(child)).GetIdentifier() != Identifier::Animation)
    {
      scene.GetComponent<ee::Sprite>(ee::Actor(child)).SetVisible(parentSpriteComp.IsVisible());
    }
  }
}

void ActorHolderSystem::UpdateChildrensTransform(ee::Scene& scene, const ActorHolderComponent& parentComponent)
{
  const auto& children = parentComponent.GetChildren();
  const auto& parentTransComp = scene.GetComponent<ee::Transform>(parentComponent.actor);

  for (auto child : children)
  {
    if (scene.HasComponent<ee::Transform>(ee::Actor(child)))
    {
      auto& childTransComp = scene.GetComponent<ee::Transform>(ee::Actor(child));
      childTransComp.SetPosition(parentTransComp.GetPosition());
      childTransComp.SetScale(parentTransComp.GetScale());
      if (scene.GetComponent<IdentificationComponent>(ee::Actor(child)).GetIdentifier() != Identifier::MeteorLightShaft)
      {
        childTransComp.SetRotation(parentTransComp.GetRotationAngle(), parentTransComp.GetRotationAxis());
      }
    }
  }
}

void ActorHolderSystem::UpdateChildrensMotion(ee::Scene& scene, const ActorHolderComponent& parentComponent)
{
  const auto& children = parentComponent.GetChildren();
  const auto& parentTransComp = scene.GetComponent<ee::Transform>(parentComponent.actor);

  for (auto child : children)
  {
    if (scene.HasComponent<ee::MotionTween>(ee::Actor(child)))
    {
      auto& childMotionComp = scene.GetComponent<ee::MotionTween>(ee::Actor(child));
      childMotionComp.SetStartPosition(parentTransComp.GetPosition());
      childMotionComp.SetFinishPosition(parentTransComp.GetPosition());
    }
  }
}