#include "space-defenders/System/CollisionSystem.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Command/CommandQueue.hpp"
#include "space-defenders/Utilities.hpp"

#include <elixir/game/components/Sprite.hpp>
#include <elixir/game/components/Transform.hpp>
#include <elixir/game/Actor.hpp>
#include <elixir/game/Scene.hpp>
#include <Elixir/Foundation/Pointer.hpp>
#include <elixir/math/Collision.hpp>

void CollisionSystem::Step(ee::Scene& scene, float dt)
{
  ee::Vector<CollisionComponent> playerColliders;
  ee::Vector<CollisionComponent> enemyColliders;

  UpdateBoundings(scene);
  FindColliders(scene, playerColliders, {Identifier::Missile, Identifier::Player });
  FindColliders(scene, enemyColliders, {Identifier::Meteor, Identifier::SpaceShip });

  for (auto& playerCollider : playerColliders)
  {
    HandleCollision(scene, playerCollider, enemyColliders);
  }
}

void CollisionSystem::UpdateBoundings(ee::Scene& scene)
{
  auto& colliderList = scene.GetComponentList<CollisionComponent>();

  for (auto& component : colliderList)
  {
    auto& tranfsorm = scene.GetComponent<ee::Transform>(component.actor);
    component.GetBoundingRect().TranslateTo(tranfsorm.GetPosition().x, tranfsorm.GetPosition().y);
  }
}

void CollisionSystem::FindColliders(ee::Scene& scene, ee::Vector<CollisionComponent>& colliders, const ee::Vector<Identifier>& idList)
{
  auto& colliderList = scene.GetComponentList<CollisionComponent>();

  for (const auto& collider : colliderList)
  {
    const auto& colliderId = scene.GetComponent<IdentificationComponent>(collider.actor);

    auto& comp = std::find_if(idList.begin(), idList.end(), [&colliderId, &scene](const Identifier& id)
    {
      return (colliderId.GetIdentifier() == id && !colliderId.IsMarkedAsDestory() && scene.GetComponent<ee::Sprite>(colliderId.actor).IsVisible());
    });

    if (comp != idList.end())
    {
      colliders.push_back(collider);
    }
  }
}

void CollisionSystem::HandleCollision(ee::Scene& scene, CollisionComponent& playerCollider, ee::Vector<CollisionComponent>& colliderList)
{
  auto& colliderComponent = std::find_if(colliderList.begin(), colliderList.end(), [&playerCollider, this](const CollisionComponent& enemyComponent)
  {
    return this->IsCollision(playerCollider, enemyComponent);
  });

  if (colliderComponent != colliderList.end())
  {
    StartDestroyProcedure(scene, colliderComponent->actor);
    StartDestroyProcedure(scene, playerCollider.actor);
  }
}

bool CollisionSystem::IsCollision(const CollisionComponent& player, const CollisionComponent& enemy)
{
  bool isVerticalOverlap = player.GetBoundingRect().GetTop() >= enemy.GetBoundingRect().GetBottom() && player.GetBoundingRect().GetBottom() <= enemy.GetBoundingRect().GetTop();
  bool isHorizontalOverlap = player.GetBoundingRect().GetRight() >= enemy.GetBoundingRect().GetLeft() && player.GetBoundingRect().GetLeft() <= enemy.GetBoundingRect().GetRight();

  return (isVerticalOverlap && isHorizontalOverlap);
}

void CollisionSystem::StartDestroyProcedure(ee::Scene& scene, ee::Actor actor)
{
  scene.GetComponent<IdentificationComponent>(actor).MarkToDestroy();
  auto animationId = Utilities::GetAnimationFromParent(scene, actor, AnimationType::Death); 

  if (animationId != ee::InvalidId)
  {
    scene.GetComponent<AnimationComponent>(ee::Actor(animationId)).Start();
  }
}