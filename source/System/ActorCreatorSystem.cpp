#include "space-defenders/System/ActorCreatorSystem.hpp"
#include "space-defenders/Factory/MeteorFactory.hpp"
#include "space-defenders/Factory/SpaceShipFactory.hpp"
#include "space-defenders/Command/CommandQueue.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Command/CreateActor.hpp"
#include "space-defenders/Utilities.hpp"
#include "space-defenders/Component/ControllerComponent.hpp"
#include "space-defenders/Component/GameRuleComponent.hpp"

#include <elixir/game/components/Transform.hpp>
#include <elixir/game/components/ColorTween.hpp>
#include <elixir/game/ActorList.hpp>

ActorCreatorSystem::ActorCreatorSystem(ee::Scene& scene)
  : meteorFactor_(scene)
  , missileFactory_(scene)
  , playerFactory_(scene)
  , spaceShipFactory_(scene)
{
}

void ActorCreatorSystem::Step(ee::Scene& scene, float)
{
  auto& gm = Utilities::FindActor(scene, Identifier::GameMaster);
  auto& gameComponent = scene.GetComponent<GameRuleComponent>(*gm);

  SendCommandToCreateMeteors(scene, gameComponent);
  SendCommandToCreateSpaceShips(scene, gameComponent);
  SendCommandToCreatePlayer(scene, gameComponent);
  SendCommandToCreateMissile(scene);
}

void ActorCreatorSystem::SendCommandToCreateMeteors(ee::Scene& scene, const GameRuleComponent& component)
{
  unsigned noOfMeteorsToCreate = component.GetNumberOfMeteors() - GetNumberOfEnemies(scene, Identifier::Meteor);

  for (std::size_t i = 0; i < noOfMeteorsToCreate; i++)
  {
    CommandQueue::GetInstance().AddCommand(ee::MakeUnique<CreateActor>(meteorFactor_));
  }
}

void ActorCreatorSystem::SendCommandToCreateSpaceShips(ee::Scene& scene, const GameRuleComponent& component)
{
  unsigned noOfSpaceShipsToCreate = component.GetNumberOfSpaceShips() - GetNumberOfEnemies(scene, Identifier::SpaceShip);

  for (std::size_t i = 0; i < noOfSpaceShipsToCreate; i++)
  {
    CommandQueue::GetInstance().AddCommand(ee::MakeUnique<CreateActor>(spaceShipFactory_));
  }
}

std::size_t ActorCreatorSystem::GetNumberOfEnemies(ee::Scene& scene, Identifier id)
{
   ee::Vector<ee::Actor> enemies;
   Utilities::FindActors(enemies, scene, id);

   return enemies.size();
}

void ActorCreatorSystem::SendCommandToCreatePlayer(ee::Scene& scene, GameRuleComponent& component)
{
  auto& playerSpaceShip = Utilities::FindActor(scene, Identifier::Player);
  auto numberOfLifes = component.GetNumberOfPlayerLifes();

  if (playerSpaceShip == scene.GetActorList().end() && numberOfLifes > 0)
  {
    numberOfLifes--;
    component.SetNumberOfPlayerLifes(numberOfLifes);
    CommandQueue::GetInstance().AddCommand(ee::MakeUnique<CreateActor>(playerFactory_));
  }
}

void ActorCreatorSystem::SendCommandToCreateMissile(ee::Scene& scene)
{
  auto& playerSpaceShip = Utilities::FindActor(scene, Identifier::Player);

  if (playerSpaceShip != scene.GetActorList().end())
  {
    auto& missile = Utilities::FindActor(scene, Identifier::Missile);
    auto& position = scene.GetComponent<ee::Transform>(*playerSpaceShip).GetPosition();
    auto buttonStatus = scene.GetComponent<ControllerComponent>(*playerSpaceShip).GetFireButtonState();

    if (missile == scene.GetActorList().end() && buttonStatus == ButtonState::PRESSED)
    {
      missileFactory_.SetSpeed(5.f);
      missileFactory_.SetDetonationTime(2.f);
      missileFactory_.SetTrackingTime(0.25f);
      missileFactory_.SetStartPosition(position);
      CommandQueue::GetInstance().AddCommand(ee::MakeUnique<CreateActor>(missileFactory_));
    }
  }
}