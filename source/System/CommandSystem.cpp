#include "space-defenders/System/CommandSystem.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Factory/SpaceShipFactory.hpp"
#include "space-defenders/Factory/MeteorFactory.hpp"
#include "space-defenders/Command/CommandQueue.hpp"

#include <elixir/foundation/Random.hpp>
#include <elixir/foundation/Vector.hpp>

void CommandSystem::Step(ee::Scene& scene, float)
{
  while (!CommandQueue::GetInstance().IsEmpty())
  {
    auto command = CommandQueue::GetInstance().GetCommand();
    command->execute();
  }
}

