#include "space-defenders/System/ControllerSystem.hpp"
#include "space-defenders/Component/ControllerComponent.hpp"
#include "space-defenders/Utilities.hpp"
#include "space-defenders/Command/CommandQueue.hpp"
#include "space-defenders/Factory/MissileFactory.hpp"
#include "space-defenders/Command/Commands.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"

#include <elixir/hid/HidManager.hpp>
#include <elixir/game/Scene.hpp>
#include <Elixir/ContentManager.hpp>
#include <elixir/game/Components/Sprite.hpp>

using namespace ee;

ControllerSystem::ControllerSystem(float worldHeight, float worldWidth) 
  : worldHeight_(worldHeight)
  , worldWidth_(worldWidth)
{
  ee::HidManager::GetInstance().AddKeyboardListener(this);
}

ControllerSystem::~ControllerSystem()
{
  ee::HidManager::GetInstance().GetInstance().RemoveKeyboardListener(this);
}

void ControllerSystem::Step(ee::Scene& scene, float dt)
{
  auto controllComponents = scene.GetComponentList<ControllerComponent>();

  for (auto& component : controllComponents)
  {
    if (scene.GetComponent<ee::Sprite>(component.actor).IsVisible())
    {
      HandleMovement(scene, component, dt);
      HandleFire(scene, component);
    }
  }
}

void ControllerSystem::HandleMovement(ee::Scene& scene, ControllerComponent& component, float dt)
{
  auto& transformComponent = scene.GetComponent<Transform>(component.actor);
  auto& collisionComponent = scene.GetComponent<CollisionComponent>(component.actor);

  ee::Array<float, 2> xBoundings = { { collisionComponent.GetBoundingRect().GetWidth(), worldWidth_ - collisionComponent.GetBoundingRect().GetWidth() } };
  ee::Array<float, 2> yBoundings = { { collisionComponent.GetBoundingRect().GetHeight(), worldHeight_ - collisionComponent.GetBoundingRect().GetHeight() } };

  auto newXPosition = 0.f;
  auto newYPosition = 0.f;

  if (moveLeft_)
  {
    newXPosition = CalculateNewPosition(-1 * component.GetSpeed() * dt, transformComponent.GetPosition().x, xBoundings);
  }
  else if (moveRight_)
  {
    newXPosition = CalculateNewPosition(component.GetSpeed() * dt, transformComponent.GetPosition().x, xBoundings);
  }

  if (moveUp_)
  {
    newYPosition = CalculateNewPosition(component.GetSpeed()* dt, transformComponent.GetPosition().y, yBoundings);
  }
  else if (moveDown_)
  {
    newYPosition = CalculateNewPosition(-1 * component.GetSpeed() * dt, transformComponent.GetPosition().y, yBoundings);
  }

  transformComponent.Translate(newXPosition, newYPosition);
}

float ControllerSystem::CalculateNewPosition(float newPosition, float actualPosition, ee::Array<float, 2>& boundings)
{
  return (newPosition + actualPosition > boundings[0] && newPosition + actualPosition < boundings[1]) ? newPosition : 0.f;
}

void ControllerSystem::HandleFire(ee::Scene& scene, ControllerComponent& component)
{
  if (fire_)
  {
    component.SetFireButtonState(PRESSED);
  }
  else
  {
    component.SetFireButtonState(RELEASED);
  }
}

void ControllerSystem::OnKeyPress(Key key)
{
  switch (key)
  {
    case Key::Up: moveUp_ = true; break;
    case Key::Down: moveDown_ = true; break;
    case Key::Left:moveLeft_ = true; break;
    case Key::Right:moveRight_ = true; break;
    case Key::Space: fire_ = true; break;
  }
}

void ControllerSystem::OnKeyRelease(Key key)
{
  switch (key)
  {
    case ee::Key::Up: moveUp_ = false; break;
    case ee::Key::Down: moveDown_ = false; break;
    case ee::Key::Left: moveLeft_ = false; break;
    case ee::Key::Right: moveRight_ = false; break;
    case Key::Space: fire_ = false; break;
  }
}