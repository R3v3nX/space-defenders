#include "space-defenders/System/AnimationSystem.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"
#include "space-defenders/Utilities.hpp"

#include <elixir/Game/Components/MotionTween.hpp>
#include <elixir/Game/Components/ColorTween.hpp>
#include <elixir/Game/Components/Sprite.hpp>
#include <elixir/Game/Scene.hpp>

void AnimationSystem::Step(ee::Scene & scene, float dt)
{
  auto& animationComponents = scene.GetComponentList<AnimationComponent>();
  for (auto& animation : animationComponents)
  {
    switch (animation.GetState())
    {
    case AnimationState::Start:
      StartAnimation(scene, animation);
      break;
    case AnimationState::Run:
      CheckAnimationStatus(scene, animation);
      break;
    case AnimationState::Stop:
      StopAnimation(scene, animation);
      break;
    default:
      break;
    }
  }
}

void AnimationSystem::StartAnimation(ee::Scene& scene, AnimationComponent& component)
{
  SetAnimationVisibleState(scene, component.actor, true);
  StartTweener<ee::MotionTween>(scene, component.actor);
  StartTweener<ee::ColorTween>(scene, component.actor);
  component.Run();
}

void AnimationSystem::CheckAnimationStatus(ee::Scene& scene, AnimationComponent& component)
{
  if (IsTweenerFinish<ee::MotionTween>(scene, component.actor) && 
      IsTweenerFinish<ee::ColorTween>(scene, component.actor))
  {
    component.Stop();
  }
}

void AnimationSystem::StopAnimation(ee::Scene& scene, AnimationComponent& component)
{
  if (component.GetType() != AnimationType::Death)
  {
    SetAnimationVisibleState(scene, component.actor, false);
  }

  component.Finish();
}

void AnimationSystem::SetAnimationVisibleState(ee::Scene& scene, ee::Actor actor, bool visible)
{
  scene.GetComponent<ee::Sprite>(actor).SetVisible(visible);
  auto parentActor = scene.GetComponent<ActorHolderComponent>(actor).GetParent();

  if (parentActor != ee::InvalidId)
  {
    scene.GetComponent<ee::Sprite>(ee::Actor(parentActor)).SetVisible(!visible);
  }
}