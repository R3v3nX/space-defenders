#include "space-defenders/System/ActionSystem.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Command/CommandQueue.hpp"
#include "space-defenders/Command/Commands.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"

#include <elixir/Game/components/Transform.hpp>
#include <elixir/Game//Scene.hpp>

void ActionSystem::Step(ee::Scene& scene, float dt)
{
  auto& actionList = scene.GetComponentList<ActionComponent>();

  for (auto& action : actionList)
  {
    auto& idComp = scene.GetComponent<IdentificationComponent>(action.actor);

    if (!idComp.IsMarkedAsDestory())
    {
      SetMovmentDelay(action, dt);
      StartAction(scene, action, dt);
      MarkActorToDestory(scene, action, idComp);
    }
  }
}

void ActionSystem::SetMovmentDelay(ActionComponent& action, float dt)
{
  auto currentMovmentDelay = action.GetCurrentMovementDelay();
  auto timeDelay = (currentMovmentDelay <= action.GetMovementDelay()) ? currentMovmentDelay + dt : currentMovmentDelay;
  action.SetCurrentMovementDelay(timeDelay);
}

void ActionSystem::StartAction(ee::Scene& scene, ActionComponent& action, float dt)
{
  if (action.GetCurrentMovementDelay() < action.GetMovementDelay())
  {
    return;
  }

  for (std::size_t currentAx = 0; currentAx < action.GetAxis().size(); currentAx++)
  {
    ee::Interpolatorf interpolator{ action.GetStartValue()[currentAx],
                                    action.GetEndValue()[currentAx],
                                    action.GetDurations()[currentAx],
                                    action.GetEasingFunctions()[currentAx] };

    SetTranslation(interpolator, action, scene, currentAx);
    SetCurrentRepeat(interpolator, action, currentAx);
    SwapValues(interpolator, action, currentAx);
    SetElapsedTime(interpolator, action, dt, currentAx);
  }
}

void ActionSystem::SetTranslation(ee::Interpolatorf& interpolator, ActionComponent& action, ee::Scene& scene, std::size_t currAx)
{
  auto& transform = scene.GetComponent<ee::Transform>(action.actor);

  auto translation = transform.GetPosition();
  auto& value = (action.GetAxis()[currAx] == Axis::X) ? translation.x : translation.y;

  interpolator.Step(action.GetElapsedTime());

  value = interpolator.GetValue();

  action.SetCurrentValue(value, currAx);
  transform.SetPosition(translation);
}

void ActionSystem::SetCurrentRepeat(const ee::Interpolatorf& interpolator, ActionComponent& action, std::size_t currAx)
{
  if (interpolator.IsFinish())
  {
    auto repeat = action.GetCurrentRepeats()[currAx];
    repeat = (repeat >= action.GetNumberOfRepetitions()[currAx]) ? repeat : repeat += 1;
    action.SetCurrentRepeat(repeat, currAx);
  }
}

void ActionSystem::SwapValues(const ee::Interpolatorf& interpolator, ActionComponent& action, std::size_t currAx)
{
  if (interpolator.IsFinish() && 
      action.GetCurrentRepeats()[currAx] < action.GetNumberOfRepetitions()[currAx] &&
      action.GetReverseMode()[currAx] == ReverseMode::ON)
  {
    auto startValue = action.GetStartValue()[currAx];
    action.SetStartValue(action.GetEndValue()[currAx], currAx);
    action.SetEndValue(startValue, currAx);
  }
}

void ActionSystem::SetElapsedTime(const ee::Interpolatorf& interpolator, ActionComponent& action, float dt, std::size_t currAx)
{
  if (!interpolator.IsFinish())
  {
    auto newTimeElapsed = action.GetElapsedTime() + dt;
    action.SetElapsedTime(newTimeElapsed);
  }
  else if (interpolator.IsFinish() && 
           action.GetCurrentRepeats()[currAx] < action.GetNumberOfRepetitions()[currAx])
  {
    action.SetElapsedTime(0.f);
  }
}

void ActionSystem::MarkActorToDestory(ee::Scene& scene, ActionComponent& action, IdentificationComponent& component)
{
  if (action.IsFinish())
  {
    component.MarkToDestroy();
  }
}