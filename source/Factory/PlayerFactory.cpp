#include "space-defenders/Factory/PlayerFactory.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ComponentCreator.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Component/ControllerComponent.hpp"
#include "space-defenders/Component/GameRuleComponent.hpp"
#include "space-defenders/Utilities.hpp"

#include <elixir/game/components/Transform.hpp>
#include <elixir/game/Components/ColorTween.hpp>
#include <elixir/game/Components/Sprite.hpp>
#include <elixir/game/Components/Emitter.hpp>
#include <elixir/graphics/Material.hpp>
#include <elixir/math/Vector3.hpp>
#include <elixir/ContentManager.hpp>

PlayerFactory::PlayerFactory(ee::Scene& scene)
  : scene_(scene)
{
}

void PlayerFactory::Create()
{
  auto& gm = Utilities::FindActor(scene_, Identifier::GameMaster);
  auto& position = scene_.GetComponent<GameRuleComponent>(*gm).GetPlayerRespawnPosition();

  ee::Actor player = CreatePlayer(position);
  ee::Actor deathAnimation = CreateDeathAnimation(position);
  ee::Actor respawnAnimation = CreateRespawnAnimation(position);

  ActorHolderComponent holderPropertiesForParent;
  holderPropertiesForParent.AddChild(deathAnimation.GetId());
  holderPropertiesForParent.AddChild(respawnAnimation.GetId());

  ActorHolderComponent holderPropertiesForChildren;
  holderPropertiesForChildren.SetParent(player.GetId());

  ComponentCreator::CreateActorHolderComponent(scene_, player, holderPropertiesForParent);
  ComponentCreator::CreateActorHolderComponent(scene_, deathAnimation, holderPropertiesForChildren);
  ComponentCreator::CreateActorHolderComponent(scene_, respawnAnimation, holderPropertiesForChildren);
}

ee::Actor PlayerFactory::CreatePlayer(const ee::Vec3f& position)
{
  auto actor = scene_.CreateActor();
  const auto& textureRect = ee::Content.GetMaterial(materialName_).GetBaseTextureRect();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.f, 0.5f, 0.f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(true);

  ee::Transform transformProperties;
  transformProperties.SetPosition(position);

  ControllerComponent controllerProperties;
  controllerProperties.SetSpeed(550.f);

  CollisionComponent boundingRectProperties;
  boundingRectProperties.SetBoundingRect(ee::Rectanglef(0.5f, 0.5f, textureRect.GetHalfWidth(), textureRect.GetHalfHeight()));

  ee::Emitter emitterProperties;
  emitterProperties.SetStartSpeed(100.f);
  emitterProperties.SetAcceleration(ee::Vec3f(150.f, 0.f, 0.f));
  emitterProperties.SetStartScale(0.f);
  emitterProperties.SetEndScale(1.f);
  emitterProperties.SetEmissionRate(5.f);
  emitterProperties.SetLifetime(1.f);
  emitterProperties.SetStartColor(ee::Color::Gray);
  emitterProperties.SetEndColor(ee::Color::White);
  emitterProperties.SetMaterial(ee::Content.GetMaterial("RingParticle.emt"));
  
  ComponentCreator::CreateSpriteComponent(scene_, actor, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, actor, Identifier::Player);
  ComponentCreator::CreateTransformComponent(scene_, actor, transformProperties);
  ComponentCreator::CreateControllerComponent(scene_, actor, controllerProperties);
  ComponentCreator::CreateBoundingRect(scene_, actor, boundingRectProperties);
  ComponentCreator::CreateEmitterComponent(scene_, actor, emitterProperties);

  return actor;
}

ee::Actor PlayerFactory::CreateDeathAnimation(const ee::Vec3f& position)
{
  auto animation = scene_.CreateActor();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.f, 0.5f, 0.f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(false);

  ee::Emitter emitterProperties;
  emitterProperties.SetStartSpeed(100.f);
  emitterProperties.SetAcceleration(ee::Vec3f(150.f, 150.f, 0.f));
  emitterProperties.SetAccelerationVariation(ee::Vec3f(50.f, 25.f, 0.f));
  emitterProperties.SetStartSpeedVariation(25.f);
  emitterProperties.SetStartAngleVariation(ee::pi);
  emitterProperties.SetStartScale(0.1f);
  emitterProperties.SetEndScale(1.f);
  emitterProperties.SetEndScaleVariation(1.f);
  emitterProperties.SetEmissionRate(25.f);
  emitterProperties.SetLifetime(1.5f);
  emitterProperties.SetStartAngle(ee::pi);
  emitterProperties.SetStartColor(ee::Color::White);
  emitterProperties.SetEndColor(ee::Color::Black);
  emitterProperties.SetMaterial(ee::Content.GetMaterial("Particles.emt"));

  ee::ColorTween colorTweenProperties;
  colorTweenProperties.SetTweenType(ee::TweenType::Linear);
  colorTweenProperties.SetDuration(1000);
  colorTweenProperties.SetStartColor(ee::Color{255, 255, 255});
  colorTweenProperties.SetFinishColor(ee::Color::Black);

  ee::MotionTween motionTweenProperties;
  motionTweenProperties.SetTweenType(ee::TweenType::Linear);
  motionTweenProperties.SetDuration(1500);
  motionTweenProperties.SetStartScale(1.f);
  motionTweenProperties.SetFinishScale(0.f);
  motionTweenProperties.SetStartPosition(position);
  motionTweenProperties.SetFinishPosition(position);

  AnimationComponent animationProperties;
  animationProperties.SetType(AnimationType::Death);

  ee::Transform transformProperties;
  transformProperties.SetPosition(position);

  ComponentCreator::CreateSpriteComponent(scene_, animation, spriteProperties);
  ComponentCreator::CreateAnimationComponent(scene_, animation, animationProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, animation, Identifier::Animation);
  ComponentCreator::CreateTransformComponent(scene_, animation, transformProperties);
  ComponentCreator::CreateColorTween(scene_, animation, colorTweenProperties);
  ComponentCreator::CreateMotionTween(scene_, animation, motionTweenProperties);

  scene_.GetComponent<ee::ColorTween>(animation).SetStartAlpha(100);
  scene_.GetComponent<ee::ColorTween>(animation).SetFinishAlpha(0);
  
  return animation;
}

ee::Actor PlayerFactory::CreateRespawnAnimation(const ee::Vec3f& position)
{
  auto animation = scene_.CreateActor();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.f, 0.5f, 0.f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(false);

  ee::Transform transformProperties;
  transformProperties.SetPosition(position);

  ee::ColorTween colorTweenProperties;
  colorTweenProperties.SetTweenType(ee::TweenType::Linear);
  colorTweenProperties.SetDuration(1000);
  colorTweenProperties.SetStartColor(ee::Color::Black);
  colorTweenProperties.SetFinishColor(ee::Color{255, 255, 255});

  ee::MotionTween motionTweenProperties;
  motionTweenProperties.SetTweenType(ee::TweenType::Linear);
  motionTweenProperties.SetDuration(1500);
  motionTweenProperties.SetStartScale(0.f);
  motionTweenProperties.SetFinishScale(1.f);
  motionTweenProperties.SetStartPosition(position);
  motionTweenProperties.SetFinishPosition(position);

  AnimationComponent animationProperties;
  animationProperties.SetType(AnimationType::Respawn);

  ComponentCreator::CreateColorTween(scene_, animation, colorTweenProperties);
  ComponentCreator::CreateSpriteComponent(scene_, animation, spriteProperties);
  ComponentCreator::CreateAnimationComponent(scene_, animation, animationProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, animation, Identifier::Animation);
  ComponentCreator::CreateTransformComponent(scene_, animation, transformProperties);
  ComponentCreator::CreateMotionTween(scene_, animation, motionTweenProperties);

  scene_.GetComponent<ee::ColorTween>(animation).SetStartAlpha(0);
  scene_.GetComponent<ee::ColorTween>(animation).SetFinishAlpha(100);
  scene_.GetComponent<AnimationComponent>(animation).Start();

  return animation;
}
