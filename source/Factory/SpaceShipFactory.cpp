#include "space-defenders/Factory/SpaceShipFactory.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ComponentCreator.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"

#include <elixir/graphics/Material.hpp>
#include <elixir/foundation/Random.hpp>
#include <elixir/game/Components/Transform.hpp>
#include <elixir/game/Components/MotionTween.hpp>
#include <elixir/game/Components/ColorTween.hpp>
#include <elixir/game/Components/Emitter.hpp>
#include <elixir/game/Components/Sprite.hpp>
#include <elixir/ContentManager.hpp>

SpaceShipFactory::SpaceShipFactory(ee::Scene& scene)
  : scene_(scene)
{
}

void SpaceShipFactory::Create()
{
  const ee::Vec3f& startPosition = { ee::Random<float>(2000.f, 2600.f), ee::Random<float>(-300.f, -600.f), 0.f };

  ee::Actor animation = CreateAnimation(startPosition);
  ee::Actor spaceShip = CreateSpaceShip(startPosition);

  ActorHolderComponent holderPropertiesForParent;
  holderPropertiesForParent.AddChild(animation.GetId());

  ActorHolderComponent holderPropertiesForChildren;
  holderPropertiesForChildren.SetParent(spaceShip.GetId());

  ComponentCreator::CreateActorHolderComponent(scene_, spaceShip, holderPropertiesForParent);
  ComponentCreator::CreateActorHolderComponent(scene_, animation, holderPropertiesForChildren);
}

ee::Actor SpaceShipFactory::CreateAnimation(const ee::Vec3f& startPosition)
{
  auto animation = scene_.CreateActor();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(false);

  ee::Emitter emitterProperties;
  emitterProperties.SetStartSpeed(100.f);
  emitterProperties.SetAcceleration(ee::Vec3f(150.f, 150.f, 0.f));
  emitterProperties.SetAccelerationVariation(ee::Vec3f(50.f, 25.f, 0.f));
  emitterProperties.SetStartSpeedVariation(25.f);
  emitterProperties.SetStartAngleVariation(ee::pi);
  emitterProperties.SetStartScale(0.1f);
  emitterProperties.SetEndScale(1.f);
  emitterProperties.SetEndScaleVariation(1.f);
  emitterProperties.SetEmissionRate(25.f);
  emitterProperties.SetLifetime(1.5f);
  emitterProperties.SetStartAngle(ee::pi);
  emitterProperties.SetStartColor(ee::Color::White);
  emitterProperties.SetEndColor(ee::Color::Black);
  emitterProperties.SetMaterial(ee::Content.GetMaterial("Particles.emt"));

  ee::ColorTween colorTweenProperties;
  colorTweenProperties.SetTweenType(ee::TweenType::Linear);
  colorTweenProperties.SetDuration(1000);
  colorTweenProperties.SetStartColor(ee::Color{ 255, 255, 255 });
  colorTweenProperties.SetFinishColor(ee::Color::Black);

  ee::MotionTween motionTweenProperties;
  motionTweenProperties.SetTweenType(ee::TweenType::Linear);
  motionTweenProperties.SetDuration(1500);
  motionTweenProperties.SetStartScale(1.f);
  motionTweenProperties.SetFinishScale(0.f);
  motionTweenProperties.SetStartPosition(startPosition);
  motionTweenProperties.SetFinishPosition(startPosition);

  AnimationComponent animationProperties;
  animationProperties.SetType(AnimationType::Death);

  ee::Transform transformProperties;
  transformProperties.SetPosition(startPosition);

  ComponentCreator::CreateSpriteComponent(scene_, animation, spriteProperties);
  ComponentCreator::CreateAnimationComponent(scene_, animation, animationProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, animation, Identifier::Animation);
  ComponentCreator::CreateTransformComponent(scene_, animation, transformProperties);
  ComponentCreator::CreateColorTween(scene_, animation, colorTweenProperties);
  ComponentCreator::CreateMotionTween(scene_, animation, motionTweenProperties);

  scene_.GetComponent<ee::ColorTween>(animation).SetStartAlpha(100);
  scene_.GetComponent<ee::ColorTween>(animation).SetFinishAlpha(0);

  return animation;
}

ee::Actor SpaceShipFactory::CreateSpaceShip(const ee::Vec3f& startPosition)
{
  auto spaceShip = scene_.CreateActor();
  const ee::Vec3f endPosition = { -2300.f, 1200.f, 0.f };

  const float flyDelay = ee::Random<float>(5.f, 20.f);
  const float flyDuration = ee::Random<float>(15.f, 20.f);

  const unsigned numberOfRepeatsX = 0;
  const unsigned numberOfRepeatsY = 1;

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(true);

  ee::Transform transform;
  transform.SetPosition(startPosition);

  CollisionComponent boundingRectProperties;
  boundingRectProperties.SetBoundingRect(ee::Rectanglef(0.f, 0.f, 
    ee::Content.GetMaterial(materialName_).GetBaseTextureRect().GetHalfWidth(),
    ee::Content.GetMaterial(materialName_).GetBaseTextureRect().GetHalfHeight()));

  ActionComponent actionProperties;
  actionProperties.SetStartValue({ { startPosition.x, startPosition.y } });
  actionProperties.SetEndValue({ { endPosition.x, endPosition.y } });
  actionProperties.SetDurations({ { flyDuration, flyDuration * 0.5f } });
  actionProperties.SetMovementDelay(flyDelay);
  actionProperties.SetNumberOfRepetitions({ { numberOfRepeatsX, numberOfRepeatsY } });
  actionProperties.SetEasingFunctions({ { ee::LinearEase, ee::CubicEaseInOut } });
  actionProperties.SetReverseModes({ { ReverseMode::OFF, ReverseMode::ON } });
  actionProperties.SetAxis({ { Axis::X, Axis::Y } });

  ComponentCreator::CreateSpriteComponent(scene_, spaceShip, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, spaceShip, Identifier::SpaceShip);
  ComponentCreator::CreateTransformComponent(scene_, spaceShip, transform);
  ComponentCreator::CreateBoundingRect(scene_, spaceShip, boundingRectProperties);
  ComponentCreator::CreateAction(scene_, spaceShip, actionProperties);

  return spaceShip;
}