#include "space-defenders/Factory/MissileFactory.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/HomingMissileComponent.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/ComponentCreator.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"

#include <elixir/graphics/Material.hpp>
#include <elixir/game/components/Emitter.hpp>
#include <elixir/game/components/Transform.hpp>
#include <elixir/game/Scene.hpp>
#include <elixir/ContentManager.hpp>

MissileFactory::MissileFactory(ee::Scene& scene)
  : scene_(scene)
{
}

void MissileFactory::SetSpeed(float speed)
{
  speed_ = speed;
}

void MissileFactory::SetStartPosition(const ee::Vec3f& position)
{
  startPosition_ = position;
}

void MissileFactory::SetStartDelay(float delay)
{
  delay_ = delay;
}
void MissileFactory::SetDetonationTime(float time)
{
  detonationTime_ = time;
}

void MissileFactory::SetTrackingTime(float time)
{
  trackingTime_ = time;
}

void MissileFactory::Create()
{
  ee::Actor animation = CreateAnimation();
  ee::Actor missile = CreateMissile();

  ActorHolderComponent holderPropertiesForParent;
  holderPropertiesForParent.AddChild(animation.GetId());

  ActorHolderComponent holderPropertiesForChildren;
  holderPropertiesForChildren.SetParent(missile.GetId());

  ComponentCreator::CreateActorHolderComponent(scene_, missile, holderPropertiesForParent);
  ComponentCreator::CreateActorHolderComponent(scene_, animation, holderPropertiesForChildren);
}

ee::Actor MissileFactory::CreateAnimation()
{
  auto animation = scene_.CreateActor();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(false);

  ee::Emitter emitterProperties;
  emitterProperties.SetStartSpeed(100.f);
  emitterProperties.SetAcceleration(ee::Vec3f(150.f, 150.f, 0.f));
  emitterProperties.SetAccelerationVariation(ee::Vec3f(50.f, 25.f, 0.f));
  emitterProperties.SetStartSpeedVariation(25.f);
  emitterProperties.SetStartAngleVariation(ee::pi);
  emitterProperties.SetStartScale(0.1f);
  emitterProperties.SetEndScale(1.f);
  emitterProperties.SetEndScaleVariation(1.f);
  emitterProperties.SetEmissionRate(25.f);
  emitterProperties.SetLifetime(1.5f);
  emitterProperties.SetStartAngle(ee::pi);
  emitterProperties.SetStartColor(ee::Color::White);
  emitterProperties.SetEndColor(ee::Color::Black);
  emitterProperties.SetMaterial(ee::Content.GetMaterial("Particles.emt"));

  ee::ColorTween colorTweenProperties;
  colorTweenProperties.SetTweenType(ee::TweenType::Linear);
  colorTweenProperties.SetDuration(250.f);
  colorTweenProperties.SetStartColor(ee::Color::White);
  colorTweenProperties.SetFinishColor(ee::Color(0.f, 0.f, 0.f, 0.f));

  AnimationComponent animationProperties;
  animationProperties.SetType(AnimationType::Death);

  ee::Transform transformProperties;
  transformProperties.SetPosition(startPosition_);

  ComponentCreator::CreateSpriteComponent(scene_, animation, spriteProperties);
  ComponentCreator::CreateAnimationComponent(scene_, animation, animationProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, animation, Identifier::Animation);
  ComponentCreator::CreateTransformComponent(scene_, animation, transformProperties);
  ComponentCreator::CreateColorTween(scene_, animation, colorTweenProperties);

  scene_.GetComponent<ee::ColorTween>(animation).SetStartAlpha(100);
  scene_.GetComponent<ee::ColorTween>(animation).SetFinishAlpha(0);

  return animation;
}

ee::Actor MissileFactory::CreateMissile()
{
  auto missile = scene_.CreateActor();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(materialName_));
  spriteProperties.SetVisible(true);

  ee::Transform transformProperties;
  transformProperties.SetPosition(startPosition_);
  transformProperties.SetRotation(4.7f, ee::Vec3f(0.f, 0.f, 1.f));

  HommingMissileComponent homingMissileProperties;
  homingMissileProperties.SetSpeed(speed_);
  homingMissileProperties.SetTimeTracking(trackingTime_);
  homingMissileProperties.SetAutoDestructionTime(detonationTime_);

  CollisionComponent boundingRectProperties;
  boundingRectProperties.SetBoundingRect(ee::Rectanglef(0.f, 0.f,
    ee::Content.GetMaterial(materialName_).GetBaseTextureRect().GetHalfWidth(),
    ee::Content.GetMaterial(materialName_).GetBaseTextureRect().GetHalfHeight()));

  ee::Emitter emitterProperties;
  emitterProperties.SetStartSpeed(50.f);
  emitterProperties.SetAcceleration(ee::Vec3f(50.f, 50.f, 0.f));
  emitterProperties.SetAccelerationVariation(ee::Vec3f(50.f, 25.f, 0.f));
  emitterProperties.SetStartSpeedVariation(25.f);
  emitterProperties.SetStartAngleVariation(ee::pi8);
  emitterProperties.SetStartScale(0.f);
  emitterProperties.SetEndScale(0.5f);
  emitterProperties.SetEndScaleVariation(0.5f);
  emitterProperties.SetEmissionRate(100.f);
  emitterProperties.SetLifetime(1.5f);
  emitterProperties.SetStartAngle(ee::pi2);
  emitterProperties.SetStartColor(ee::Color::White);
  emitterProperties.SetEndColor(ee::Color(204.f, 204.f, 204.f, 0.f));
  emitterProperties.SetMaterial(ee::Content.GetMaterial("Particles.emt"));

  ComponentCreator::CreateSpriteComponent(scene_, missile, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, missile, Identifier::Missile);
  ComponentCreator::CreateTransformComponent(scene_, missile, transformProperties);
  ComponentCreator::CreateHomingMissileComponent(scene_, missile, homingMissileProperties);
  ComponentCreator::CreateBoundingRect(scene_, missile, boundingRectProperties);
  ComponentCreator::CreateEmitterComponent(scene_, missile, emitterProperties);

  return missile;
}