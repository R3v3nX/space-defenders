#include "space-defenders/Factory/MeteorFactory.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ComponentCreator.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"

#include <elixir/game/components/Transform.hpp>
#include <elixir/game/components/Emitter.hpp>
#include <elixir/foundation/Random.hpp>
#include <elixir/foundation/String.hpp>
#include <elixir/graphics/Material.hpp>
#include <elixir/math/Rect.hpp>
#include <elixir/ContentManager.hpp>

const ee::Array<const ee::Pair<ee::Vec3f, ee::Vec3f>, 10> MeteorFactory::MeteorSpawnPoints =
{
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 1150.f, 2180.f, 100.f }, { -1000.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 1650.f, 2180.f, 100.f }, { -550.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 1950.f, 2180.f, 100.f }, { -300.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2000.f, 2180.f, 100.f }, { -150.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2150.f, 2180.f, 100.f }, { 0.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2300.f, 2180.f, 100.f }, { 150.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2450.f, 2180.f, 100.f }, { 300.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2600.f, 2180.f, 100.f }, { 450.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2750.f, 2180.f, 100.f }, { 550.f, -830.f, 100.f }),
  ee::Pair<ee::Vec3f, ee::Vec3f>({ 2900.f, 2180.f, 100.f }, { 700.f, -830.f, 100.f }),
};

MeteorFactory::MeteorFactory(ee::Scene& scene)
  : scene_(scene)
{
}

void MeteorFactory::Create()
{
  float flyDelay = ee::Random<float>(5.f, 10.f);
  float flyDuration = ee::Random<float>(3.f, 9.f);

  std::size_t index = ee::Random<unsigned>(0, MeteorSpawnPoints.size() - 1);
  const auto& spawnPoint = MeteorSpawnPoints[index];

  ee::Actor lightShaft = CreateLightShaft(spawnPoint.first, flyDuration, flyDelay);
  ee::Actor meteor = CreateMeteor(spawnPoint, flyDuration, flyDelay);
  ee::Actor animation = CreateAnimation();

  ActorHolderComponent holderPropertiesForParent;
  holderPropertiesForParent.AddChild(animation.GetId());
  holderPropertiesForParent.AddChild(lightShaft.GetId());

  ActorHolderComponent holderPropertiesForChildren;
  holderPropertiesForChildren.SetParent(meteor.GetId());

  ComponentCreator::CreateActorHolderComponent(scene_, meteor, holderPropertiesForParent);
  ComponentCreator::CreateActorHolderComponent(scene_, animation, holderPropertiesForChildren);
  ComponentCreator::CreateActorHolderComponent(scene_, lightShaft, holderPropertiesForChildren);
}

ee::Actor MeteorFactory::CreateMeteor(const ee::Pair<ee::Vec3f, ee::Vec3f>& spawnPoint, float flyDuration, float flyDelay)
{
  auto meteor = scene_.CreateActor();

  const auto& startPosition = spawnPoint.first;
  const auto& endPosition = spawnPoint.second;

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(meteorMaterialName_));
  spriteProperties.SetVisible(true);

  ee::Transform transformProperties;
  transformProperties.SetPosition({ startPosition });
  transformProperties.SetRotation(0.f, ee::Vec3f(0.f, 0.f, 1.f));

  CollisionComponent boundingRectProperties;
  boundingRectProperties.SetBoundingRect(ee::Rectanglef(0.f, 0.f,
    ee::Content.GetMaterial(meteorMaterialName_).GetBaseTextureRect().GetHalfWidth(),
    ee::Content.GetMaterial(meteorMaterialName_).GetBaseTextureRect().GetHalfHeight()));

  ActionComponent actionProperties;
  actionProperties.SetStartValue({ { startPosition.x, startPosition.y } });
  actionProperties.SetEndValue({ { endPosition.x, endPosition.y } });
  actionProperties.SetDurations({ { flyDuration, flyDuration } });
  actionProperties.SetMovementDelay(flyDelay);
  actionProperties.SetNumberOfRepetitions({ { 0, 0 } });
  actionProperties.SetEasingFunctions({ { ee::LinearEase, ee::LinearEase } });
  actionProperties.SetReverseModes({ { ReverseMode::OFF, ReverseMode::OFF } });
  actionProperties.SetAxis({ { Axis::X, Axis::Y } });

  ComponentCreator::CreateSpriteComponent(scene_, meteor, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, meteor, Identifier::Meteor);
  ComponentCreator::CreateTransformComponent(scene_, meteor, transformProperties);
  ComponentCreator::CreateBoundingRect(scene_, meteor, boundingRectProperties);
  ComponentCreator::CreateAction(scene_, meteor, actionProperties);

  return meteor;
}

ee::Actor MeteorFactory::CreateLightShaft(const ee::Vec3f& startPosition, float flyDuration, float flyDelay)
{
  auto lightShaft = scene_.CreateActor();

  const ee::Vec3f lightShaftShift = { 150.f, 220.f, 0.f };
  const ee::Vec3f lightStartPosition = startPosition + lightShaftShift;

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 1.f, 0.f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(lightShaftMaterialName_));
  spriteProperties.SetVisible(true);

  ee::Transform transform;
  transform.SetPosition({ lightStartPosition});
  transform.SetRotation(-10.f, ee::Vec3f(0.f, 0.f, 1.f));

  ComponentCreator::CreateSpriteComponent(scene_, lightShaft, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, lightShaft, Identifier::MeteorLightShaft);
  ComponentCreator::CreateTransformComponent(scene_, lightShaft, transform);

  return lightShaft;
}

ee::Actor MeteorFactory::CreateAnimation()
{
  auto animation = scene_.CreateActor();

  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial(meteorMaterialName_));
  spriteProperties.SetVisible(false);

  ee::Emitter emitterProperties;
  emitterProperties.SetStartSpeed(100.f);
  emitterProperties.SetAcceleration(ee::Vec3f(150.f, 150.f, 0.f));
  emitterProperties.SetAccelerationVariation(ee::Vec3f(50.f, 25.f, 0.f));
  emitterProperties.SetStartSpeedVariation(25.f);
  emitterProperties.SetStartAngleVariation(ee::pi);
  emitterProperties.SetStartScale(0.1f);
  emitterProperties.SetEndScale(1.f);
  emitterProperties.SetEndScaleVariation(1.f);
  emitterProperties.SetEmissionRate(25.f);
  emitterProperties.SetLifetime(1.5f);
  emitterProperties.SetStartAngle(ee::pi);
  emitterProperties.SetStartColor(ee::Color::White);
  emitterProperties.SetEndColor(ee::Color::Black);
  emitterProperties.SetMaterial(ee::Content.GetMaterial("Particles.emt"));

  ee::ColorTween colorTweenProperties;
  colorTweenProperties.SetTweenType(ee::TweenType::Linear);
  colorTweenProperties.SetDuration(100);
  colorTweenProperties.SetStartColor(ee::Color::White);
  colorTweenProperties.SetFinishColor(ee::Color(0.f, 0.f, 0.f, 0.f));

  ee::MotionTween motionTweenProperties;
  motionTweenProperties.SetTweenType(ee::TweenType::Linear);
  motionTweenProperties.SetDuration(2500);
  motionTweenProperties.SetStartScale(1.f);
  motionTweenProperties.SetFinishScale(0.f);

  AnimationComponent animationProperties;
  animationProperties.SetType(AnimationType::Death);

  ComponentCreator::CreateSpriteComponent(scene_, animation, spriteProperties);
  ComponentCreator::CreateAnimationComponent(scene_, animation, animationProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, animation, Identifier::Animation);
  ComponentCreator::CreateTransformComponent(scene_, animation, {});
  ComponentCreator::CreateColorTween(scene_, animation, colorTweenProperties);
  ComponentCreator::CreateMotionTween(scene_, animation, motionTweenProperties);

  scene_.GetComponent<ee::ColorTween>(animation).SetStartAlpha(100);
  scene_.GetComponent<ee::ColorTween>(animation).SetFinishAlpha(0);

  return animation;
}
