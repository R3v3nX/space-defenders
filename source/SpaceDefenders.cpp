#include <elixir/Application.hpp>
#include <elixir/hid/HidManager.hpp>
#include <elixir/graphics/GraphicsService.hpp>
#include <elixir/graphics/Window.hpp>

#include "space-defenders/World.hpp"

class SpaceDefenders final : public ee::Application
{
public:
  SpaceDefenders()
    : world_(designWidth, designHeight, windowWidth, windowHeight)
  {
    SwitchToWindowed();
  }

  virtual ee::String GetName() const override
  {
    return "Space Defenders";
  }

  virtual void Step() override
  {
    world_.Simulate();
  }

private:
  void SwitchToFullscreen()
  {
    // auto const displayMode = Display.GetMode();
    // Graphics.GetWindow().Resize(displayMode.GetWidth(), displayMode.GetHeight(), true);
    ee::HidManager::GetInstance().HideMouseCursor();
  }

  void SwitchToWindowed()
  {
    // Display.RestoreMode();
    ee::GraphicsService::GetInstance().GetWindow().Resize(windowWidth, windowHeight);
    ee::HidManager::GetInstance().ShowMouseCursor();
  }

  float const designWidth = 2048.f;
  float const designHeight = 1536.f;

  float const windowWidth = 1024.f;
  float const windowHeight = 768.f;

  World world_;
};

EE_APPLICATION(SpaceDefenders)