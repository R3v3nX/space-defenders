#include "space-defenders/Utilities.hpp"
#include "space-defenders/Component/AnimationComponent.hpp"

auto Utilities::FindActor(const ee::Scene& scene, Identifier id) -> ConstIterator
{
  auto& idList = scene.GetActorList();

  auto iter = std::find_if(idList.begin(), idList.end(), [&scene, id](const ee::Actor actor)
  {
    return scene.HasComponent<IdentificationComponent>(actor) &&
           scene.GetComponent<IdentificationComponent>(actor).GetIdentifier() == id;
  });

  return iter;
}

auto Utilities::FindActor(const ee::Scene& scene, const Predicate& predicate) -> ConstIterator
{
  auto& idList = scene.GetActorList();
  auto iter = std::find_if(idList.begin(), idList.end(), predicate);

  return iter;
}

void Utilities::FindActors(ee::Vector<ee::Actor>& actors, ee::Scene& scene, Identifier id)
{
  auto& idList = scene.GetComponentList<IdentificationComponent>();

  for (auto& component : idList)
  {
    if (component.GetIdentifier() == id)
    {
      actors.push_back(component.actor);
    }
  }
}

void Utilities::FindActors(ee::Vector<ee::Actor>& actors, ee::Scene& scene, const Predicate& predicate)
{
  for (auto& actor : scene.GetActorList())
  {
    if (predicate(actor))
    {
      actors.push_back(actor);
    }
  }
}

auto Utilities::GetAnimationFromParent(const ee::Scene& scene, ee::Actor parent, AnimationType animationType) -> ee::Id
{
  auto& children = scene.GetComponent<ActorHolderComponent>(parent).GetChildren();
  const auto& iter = std::find_if(children.begin(), children.end(), [&scene, animationType](ee::Id childId)
  {
    return (scene.HasComponent<AnimationComponent>(ee::Actor(childId)) && scene.GetComponent<AnimationComponent>(ee::Actor(childId)).GetType() == animationType);
  });
  
  return (iter == children.end()) ? ee::InvalidId : *iter;
}