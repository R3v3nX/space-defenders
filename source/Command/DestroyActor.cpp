#include "space-defenders/Command/DestroyActor.hpp"
#include "space-defenders/Component/ActorHolderComponent.hpp"

DestroyActor::DestroyActor(ee::Scene& scene, ee::Actor actor)
  : scene_(scene)
  , actor_(actor)
{
}

void DestroyActor::execute()
{
  if (scene_.HasComponent<ActorHolderComponent>(actor_))
  {
    auto& actors = scene_.GetComponent<ActorHolderComponent>(actor_).GetChildren();
    for (auto actor : actors)
    {
      scene_.DestroyActor(ee::Actor(actor));
    }
  }
  scene_.DestroyActor(actor_);
}