#include "space-defenders/Command/CreateActor.hpp"
#include "space-defenders/Factory/IActorFactory.hpp"

CreateActor::CreateActor(IActorFactory& factory)
  : factory_(factory)
{
}


void CreateActor::execute()
{
  factory_.Create();
}