#include "space-defenders/Command/Command.hpp"

Command::Command(const std::function<void()>& action)
  : action_(action)
{
}

void Command::execute()
{
  action_();
}
