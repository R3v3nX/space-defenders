#include "space-defenders/Command/CommandQueue.hpp"

CommandQueue& CommandQueue::GetInstance()
{
  static CommandQueue instance_;
  return instance_;
}

void CommandQueue::AddCommand(ee::UniquePtr<ICommand> command)
{
  queue_.push(std::move(command));
}

ee::UniquePtr<ICommand> CommandQueue::GetCommand()
{
  ee::UniquePtr<ICommand> cmd = std::move(queue_.top());
  queue_.pop();

  return cmd;
}

std::size_t CommandQueue::GetSize() const
{
  return queue_.size();
}

bool CommandQueue::IsEmpty() const
{
  return queue_.empty();
}