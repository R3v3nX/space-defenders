#include "space-defenders/Command/Commands.hpp"
#include "space-defenders/Component/ActionComponent.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/CollisionComponent.hpp"
#include "space-defenders/Factory/SpaceShipFactory.hpp"
#include "space-defenders/Factory/MeteorFactory.hpp"
#include "space-defenders/Factory/PlayerFactory.hpp"
#include "space-defenders/Utilities.hpp"
#include "space-defenders/Component/HomingMissileComponent.hpp"
#include "space-defenders/Factory/MissileFactory.hpp"
#include "space-defenders/Component/ComponentCreator.hpp"

#include <elixir/game/components/Sprite.hpp>
#include <elixir/game/components/Transform.hpp>
#include <elixir/game/components/ColorTween.hpp>
#include <elixir/game/components/MotionTween.hpp>
#include <elixir/game/Components/Emitter.hpp>
#include <elixir/game/Scene.hpp>

#include <elixir/math/Vector3.hpp>

#include <elixir/foundation/Random.hpp>

#include <elixir/ContentManager.hpp>

/*
ICommand CreatePlayer(ee::Scene& scene)
{
  std::function<void()> cmd = [&scene]
  {
    PlayerFactory player(scene);
    player.Create();
  };

  return ICommand(cmd);
}

ICommand CreateSpaceShip(ee::Scene& scene, const ee::String& materialName)
{
  std::function<void()> cmd = [&scene, materialName]
  {
    SpaceShipFactory spaceShipCreator(scene, materialName);
    spaceShipCreator.Create();
  };

  return ICommand(cmd);
}

ICommand CreateMeteor(ee::Scene& scene)
{
  std::function<void()> cmd = [&scene]
  {
    MeteorFactory meteorCreator(scene);
    meteorCreator.Create();
  };

  return ICommand(cmd);
}

ICommand CreateHommingMissile(ee::Scene& scene, const ee::Vec3f& position)
{
  std::function<void()> cmd = [&scene, position]
  {
    MissileFactory missile(scene, "Missile.emt");
    missile.SetSpeed(5.f);
    missile.SetDetonationTime(2.f);
    missile.SetTrackingTime(0.25f);
    missile.SetStartPosition(position);
    missile.Create();
  };

  return ICommand(cmd);
}

ICommand StartRespawnAnimation(ee::Scene& scene, ee::Actor actor)
{
  auto transformComponent = scene.GetComponent<ee::Transform>(actor);
  auto textureId = scene.GetComponent<ee::Sprite>(actor).GetMaterial();

  std::function<void()> cmd = [&scene, transformComponent, textureId]
  {
    ee::Transform transformProperties;
    transformProperties.SetPosition(transformComponent.GetPosition());

    ee::ColorTween colorTweenProperties;
    colorTweenProperties.SetTweenType(ee::TweenType::Linear);
    colorTweenProperties.SetDuration(1000);
    colorTweenProperties.SetStartColor(ee::Color(0.f, 0.f, 0.f, 0.f));
    colorTweenProperties.SetFinishColor(ee::Color::White);

    auto actor = scene.CreateActor();
    ComponentCreator::CreateSpriteComponent(scene, actor, textureId);
    ComponentCreator::CreateTransformComponent(scene, actor, transformProperties);
    ComponentCreator::CreateColorTween(scene, actor, colorTweenProperties);
    ComponentCreator::CreateIdentificationComponent(scene, actor, Identifier::Animation);

    scene.GetComponent<ee::Sprite>(actor).SetAnchor(0.5f, 0.5f, 0.f);
    scene.GetComponent<IdentificationComponent>(actor).MarkToDestroy();
  };

  return ICommand(cmd);
}

ICommand StartDestroyAnimation(ee::Scene& scene, ee::Actor actor)
{
  auto transformComponent = scene.GetComponent<ee::Transform>(actor);
  auto textureId = scene.GetComponent<ee::Sprite>(actor).GetMaterial();

  std::function<void()> cmd = [&scene, transformComponent, textureId]
  {
    ee::Transform transformProperties;
    transformProperties.SetPosition(transformComponent.GetPosition());
    transformProperties.SetRotation(transformProperties.GetRotationAngle(), transformProperties.GetRotationAxis());
    transformProperties.SetScale(transformProperties.GetScale());

    ee::Emitter emitterProperties;
    emitterProperties.SetStartSpeed(100.f);
    emitterProperties.SetAcceleration(ee::Vec3f(150.f, 150.f, 0.f));
    emitterProperties.SetAccelerationVariation(ee::Vec3f(50.f, 25.f, 0.f));
    emitterProperties.SetStartSpeedVariation(25.f);
    emitterProperties.SetStartAngleVariation(ee::pi);
    emitterProperties.SetStartScale(0.1f);
    emitterProperties.SetEndScale(1.f);
    emitterProperties.SetEndScaleVariation(1.f);
    emitterProperties.SetEmissionRate(25.f);
    emitterProperties.SetLifetime(1.5f);
    emitterProperties.SetStartAngle(ee::pi);
    emitterProperties.SetStartColor(ee::Color::White);
    emitterProperties.SetEndColor(ee::Color::Black);
    emitterProperties.SetMaterial(ee::Content.GetMaterial("Particles.emt"));

    ee::ColorTween colorTweenProperties;
    colorTweenProperties.SetTweenType(ee::TweenType::Linear);
    colorTweenProperties.SetDuration(100);
    colorTweenProperties.SetStartColor(ee::Color::White);
    colorTweenProperties.SetFinishColor(ee::Color(0.f, 0.f, 0.f, 0.f));

    ee::MotionTween motionTweenProperties;
    motionTweenProperties.SetTweenType(ee::TweenType::Linear);
    motionTweenProperties.SetDuration(2500);
    motionTweenProperties.SetStartScale(1.f);
    motionTweenProperties.SetFinishScale(0.f);
    motionTweenProperties.SetStartPosition(transformComponent.GetPosition());
    motionTweenProperties.SetFinishPosition(transformComponent.GetPosition());

    auto actor = scene.CreateActor();
    ComponentCreator::CreateSpriteComponent(scene, actor, textureId);
    ComponentCreator::CreateTransformComponent(scene, actor, transformProperties);
    ComponentCreator::CreateEmitterComponent(scene, actor, emitterProperties);
    ComponentCreator::CreateColorTween(scene, actor, colorTweenProperties);
    ComponentCreator::CreateMotionTween(scene, actor, motionTweenProperties);
    ComponentCreator::CreateIdentificationComponent(scene, actor, Identifier::Animation);

    scene.GetComponent<ee::Sprite>(actor).SetAnchor(0.5f, 0.5f, 0.f);
    scene.GetComponent<IdentificationComponent>(actor).MarkToDestroy();
  };

  return ICommand(cmd);
}

ICommand DestroyActor(ee::Scene& scene, ee::Actor actor)
{
  std::function<void()> cmd = [&scene, actor]
  {
    scene.DestroyActor(actor);
  };

  return ICommand(cmd);
}
*/