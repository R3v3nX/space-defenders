#include "space-defenders/World.hpp"
#include "space-defenders/Component/IdentificationComponent.hpp"
#include "space-defenders/Component/ControllerComponent.hpp"
#include "space-defenders/Component/ComponentCreator.hpp"
#include "space-defenders/Component/GameRuleComponent.hpp"

#include <elixir/system//TimeService.hpp>
#include <elixir/graphics/GraphicsService.hpp>
#include <elixir/graphics/Window.hpp>
#include <elixir/ContentManager.hpp>

World::World(float designWidth, float designHeight, float windowWidth, float windowHeight)
  : designWidth_(designWidth)
  , designHeight_(designHeight)
  , windowWidth_(windowWidth)
  , windowHeight_(windowHeight)
  , controllerSystem_(designHeight, designWidth)
  , actorCreatorSystem_(scene_)
{
  SetUpScene();
  SetUpSystems();
  SetUpActors();
  SetUpCamera();
}

void World::SetUpScene()
{
  director_.SetScene(scene_);
}

void World::SetUpSystems()
{
  director_.AddSimulationSystem(commandSystem_);
  director_.AddSimulationSystem(controllerSystem_);
  director_.AddSimulationSystem(missileControlSystem_);
  director_.AddSimulationSystem(actionSystem_);
  director_.AddSimulationSystem(actorHolderSystem_);
  director_.AddSimulationSystem(collisionSystem_);
  director_.AddSimulationSystem(particleSystem_);
  director_.AddSimulationSystem(actorCleanupSystem_);
  director_.AddSimulationSystem(actorCreatorSystem_);
  director_.AddSimulationSystem(animationSystem_);
  director_.AddSimulationSystem(tweenSystem_);
  director_.AddRenderSystem(spriteSystem_);
  director_.AddRenderSystem(particleSystem_);
}

void World::SetUpActors()
{
  SetUpGameMaster();
  SetUpBackground();
  //SetUpPlanet();
  SetUpRocks();
}

void World::SetUpGameMaster()
{
  GameRuleComponent gameComponentProperties;

  auto actor = scene_.CreateActor();
  gameComponentProperties.SetNumberOfMeteors(5);
  gameComponentProperties.SetNumberOfSpaceShips(10);
  gameComponentProperties.SetScore(0);
  gameComponentProperties.SetNumberOfPlayerLifes(3);

  ComponentCreator::CreateIdentificationComponent(scene_, actor, Identifier::GameMaster);
  ComponentCreator::CreateGameComponent(scene_, actor, gameComponentProperties);
}

void World::SetUpBackground()
{
  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial("Background.emt"));
  spriteProperties.SetVisible(true);

  ee::Transform transformProperties;
  transformProperties.SetPosition({ windowWidth_, windowHeight_, 0.f });

  auto actor = scene_.CreateActor();
  ComponentCreator::CreateSpriteComponent(scene_, actor, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, actor, Identifier::Background);
  ComponentCreator::CreateTransformComponent(scene_, actor, transformProperties);
}

void World::SetUpRocks()
{
  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial("Rocks.emt"));
  spriteProperties.SetVisible(true);

  ee::Transform transformProperties;
  transformProperties.SetPosition({ windowWidth_ + 600.f, windowHeight_ - 500.f, 0.f });
  transformProperties.SetRotation(0.45f, ee::Vec3f(0.f, 0.f, 1.f));

  auto actor = scene_.CreateActor();
  ComponentCreator::CreateSpriteComponent(scene_, actor, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, actor, Identifier::Rocks);
  ComponentCreator::CreateTransformComponent(scene_, actor, transformProperties);
}

void World::SetUpPlanet()
{
  ee::Sprite spriteProperties;
  spriteProperties.SetAnchor(0.5f, 0.5f, 0.5f);
  spriteProperties.SetMaterial(ee::Content.GetMaterial("Planet.emt"));
  spriteProperties.SetVisible(true);

  ee::Transform transformProperties;
  transformProperties.SetPosition({ windowWidth_, windowHeight_ - 1200.f, 0.f });

  auto actor = scene_.CreateActor();
  ComponentCreator::CreateSpriteComponent(scene_, actor, spriteProperties);
  ComponentCreator::CreateIdentificationComponent(scene_, actor, Identifier::Planet);
  ComponentCreator::CreateTransformComponent(scene_, actor, transformProperties);
}

void World::SetUpCamera()
{
  camera_.SetViewRect(ee::Rectf(0.f, 0.f, windowWidth_, windowHeight_));
  camera_.EnableContentScale(ee::OrthoCamera::ContentScaleMode::AspectFill, designWidth_, designHeight_);
  director_.SetCamera(camera_);
}

void World::Simulate()
{
  director_.Step(ee::TimeService::GetInstance().GetDeltaFloat());
}
